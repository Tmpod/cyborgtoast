#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import json
import logging
import os
import pprint
import sys

import jinja2
import markdown

TEMPLATES = 'command-ref-templates'


logging.basicConfig(level='INFO')
logging.root.name = 'build-command-reference.py'
page_name_suffix = '.html'


output_dir_name = ''
try:
    output_dir_name = sys.argv[1]
except IndexError:
    print('Please provide the output directory name to dump to as the first argument.')
    exit(4)
else:
    try:
        os.mkdir(output_dir_name)
        logging.info('Created %s dir for output', output_dir_name)
    except FileExistsError:
        logging.info('Reusing existing %s dir for output', output_dir_name)

logging.info('Jinja2 v%s', jinja2.__version__)
logging.info('Markdown v%s', markdown.__version__)

logging.info('Waiting for stdin to close')
raw_command_meta = json.load(sys.stdin)
logging.info('Stdin closed, will begin rendering HTML content now')

j2 = jinja2.Environment(
    loader=jinja2.PackageLoader(__name__, TEMPLATES),
    autoescape=jinja2.select_autoescape(['xml'])
)


def render_page(template_name, *args, **kwargs):
    now = datetime.utcnow()
    return j2.get_template(template_name).render(*args, now=now, **kwargs)


# Map of all reference IDs to command metadata dicts
refs = {c['ref']: c for c in raw_command_meta}

# Root commands have no parent.
root_commands = [c for c in raw_command_meta if not c['parent']]


def get_children_recursively(command):
    for child in command['child_commands']:
        if isinstance(child, str):
            child = refs[child]
        yield child
        if child.get('child_commands'):
            yield from get_children_recursively(child)


# Resolve references to object references to make life easier, also generate urls now and their directories.
for c in raw_command_meta:
    if c['parent'] is not None:
        c['parent'] = refs[c['parent']]

    if c.get('child_commands'):
        c['child_commands'] = sorted([*get_children_recursively(c)], key=lambda c: c['name'])

    path = os.path.join(output_dir_name, '-'.join(c['fully_qualified_name'].split()) + page_name_suffix)

    path = os.path.relpath(path, output_dir_name)

    c['path'] = path


def parse_md_fields(command):
    if command['brief']:
        command['brief'] = markdown.markdown(command['brief'])

    if command['help']:
        command['help'] = markdown.markdown(command['help'])

    if command['description']:
        command['description'] = markdown.markdown(command['description'])


already_rendered = {}

for command in refs.values():
    if command in already_rendered.values():
        logging.info('%s already rendered', command['fully_qualified_name'])
    else:
        stack = [command]

        while stack:
            next_command = stack.pop()

            parse_md_fields(next_command)

            logging.info('Rendering %s', next_command['fully_qualified_name'])
            content = render_page('command-fragment.html', **next_command)
            path = os.path.join(output_dir_name, next_command['path'])
            logging.info('Dumping HTML for %s to %s', next_command['fully_qualified_name'], path)
            with open(path, 'w') as fp:
                fp.write(content)

            if next_command['is_group']:
                next_values = next_command['child_commands']
                logging.info('Will recurse to %s', [nv['fully_qualified_name'] for nv in next_values])
                stack.extend(next_values)

index = render_page('index.html', commands=sorted(refs.values(), key=lambda c: c['fully_qualified_name']))
path = os.path.join(output_dir_name, 'index.html')
logging.info('Building index.html')
with open(path, 'w') as fp:
    print(index)
    fp.write(index)
