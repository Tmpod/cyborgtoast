#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This is a discord bot with some general purpose features and other random stuff
"""

from .baker import plug

plug.PowerTap().run()
