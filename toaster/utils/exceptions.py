#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
__all__ = ("NotAdminOrCreator", "ModuleNotEnabled", "ModuleDisabled", "is_module_enabled", "admins_or_creator")

from libneko import commands


class NotAdminOrCreator(commands.CommandError):
    pass


def admins_or_creator():
    """Checks if the executor has admin pems or is the bot owner"""

    def check_body(ctx):
        if ctx.author.guild_permissions.administrator or ctx.author == ctx.bot.creator:
            return True
        else:
            raise NotAdminOrCreator(ctx.author)

    return commands.check(check_body)


class ModuleNotEnabled(commands.CommandError):
    pass


def is_module_enabled():
    async def decorator(ctx):
        if ctx.command.cog_name in await ctx.bot.redis.smembers(
            f"guilds:{ctx.guild.id}:optional_modules"
        ):
            return True
        else:
            raise ModuleNotEnabled(
                {
                    "message": f"{ctx.guild.name} (ID: {ctx.guild.id}) doesn't have {ctx.command.cog_name} enabled!",
                    "module": ctx.command.cog_name,
                }
            )

    return commands.check(decorator)


class ModuleDisabled(commands.CommandError):
    def __init__(self, ctx, module):
        self.ctx, self.module = ctx, module
