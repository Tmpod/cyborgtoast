#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
__all__ = ("GiveawayManager",)

from asyncio import sleep as asleep
from datetime import datetime
import discord
from libneko import embeds, logging
from secrets import choice

from toaster.utils import pretty_list
from . import MongoDatabase, RedisPool


class GiveawayManager(logging.Log):
    """
    Manages the database operations related to the Giveaways module
    """
    def __init__(self, redis: RedisPool, mongo: MongoDatabase):
        self.redis = redis
        self.mongo = mongo

    async def check_giveaways(self):
        """
        Checks if a giveaway has ended every 5 seconds. 
        If it has it will trigger the terminate function
        """
        while True:  # Gonna be checking as long as the bot is on
            active_gw = await self.redis.smembers("giveaways:index")
            now = datetime.utcnow().timestamp()

            for t in map(int, active_gw):
                if t < now:
                    continue

                key = f"giveaways:{t}"

                gw = await self.redis.hgetall(key)

                if not gw:
                    continue

            try:
                await self.terminate_giveaway(gw)
            except Exception as ex:
                self.bot.logger.error(ex, exc_info=True)
            else:
                await self.redis.delete(key)
                await self.redis.srem(t)

            await asleep(5)

    async def terminate_giveaway(self, giveaway):
        """
        Ends a giveaway by rolling out the winner(s) and editing the message
        """
        ch = self.bot.get_channel(int(giveaway["channel"]))
        if not ch:
            return

        try:
            m = await ch.get_message(int(giveaway["message"]))
        except discord.errors.NotFound:
            return

        if m.reactions:
            try:
                contestants = (
                    await (discord.util.get(m.reactions, emoji=self.GIVEAWAY_EMOJI))
                    .users()
                    .flatten()
                )
            except AttributeError:
                await m.add_reaction(self.bot.crossmark)

            winners = set()
            for _ in range(int(giveaway["winners"])):
                c = choice(contestants)
                if c not in winners:
                    winners.add(choice(contestants))

            emb = embeds.Embed(
                title=giveaway["prize"],
                description=(
                    "**Winners:**\n"
                    f"{pretty_list(map(lambda m: m.mention, winners), sep=self.__NL)}"
                ),
                colour=self.ENDED_COLOUR,
            )

            emb.set_footer(text="Ended at")

            try:
                await m.edit(embed=emb)
            except (discord.Forbidden, discord.errors.NotFound):
                self.logger.error("Tried ending a giveaway but couldnt!")
