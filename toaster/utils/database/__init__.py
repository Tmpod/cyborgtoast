#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
from .tags import *


from aioredis import Redis
from motor.motor_asyncio import AsyncIOMotorDatabase
from typing import NewType

# MongoDatabase = NewType("MongoDatabase", AsyncIOMotorDatabase)
# RedisPool = NewType("RedisPool", Redis)
