#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
__all__ = (
    "rand_hex",
    "get_log_num",
    "get_ext_path",
    "repeat",
    "list_files",
    "visual_switch",
    "chk_or_crs",
    "run_in_shell",
    "command_not_found",
    "get_prefix",
    "confirm_opts",
    "confirm_opts_full",
    "discord_input",
    "closable",
    "replace_recursive",
    "embed_replace",
    "with_category",
    "subtract_list",
    "success_embed",
)

import discord
from random import randint
import asyncio
from libneko import embeds, converters
import os
from functools import wraps as ftwraps
from typing import Tuple, Union, Callable
import inspect
import logging


def rand_hex() -> int:
    """Creates a random colour."""
    return randint(0, 0xFFFFFF)


def get_log_num(logs_dir: str = "/breadvan/logs") -> str:
    """Gets correct log file number"""
    latest_log_num = max({int(l[7:-4]) for l in os.listdir(logs_dir)} or {0})
    current_log_num = int(str(latest_log_num + 1).zfill(3))
    return current_log_num


async def get_ext_path(bot, ext_name: str) -> str:
    """Gets the full path for the given ext name"""
    async with bot.baker.rsc.get("extensions.txt") as fp:
        exts_file = (await fp.read()).strip().split("\n")
    exts = {
        f"toaster.exts.{l[1:]}" if l.startswith("*") else l.strip()
        for l in exts_file
        if l and not l.startswith("#")
    }
    ### See toaster.baker.mold for the extended version of this set comprehension ###
    if ext_name.lower() == "all":
        return exts
    for ext in exts:
        if ext.endswith(ext_name):
            return ext


def repeat(sleep: int = 0):
    def decorator(coro):
        @ftwraps(coro)
        async def repeater():
            while True:
                await coro()
                await asyncio.sleep(sleep)

        return repeater

    return decorator


def list_files(path, *extras, abs=False):
    p = os.path.join(path, *extras)
    if abs:
        p = os.path.abspath(p)

    for file in os.listdir(p):
        yield os.path.join(p, file)


def visual_switch(val: bool) -> str:
    return (
        "<:on_switch:471655734417948682>"
        if bool(val)
        else "<:off_switch:471657039009939457>"
    )


def chk_or_crs(val: bool) -> str:
    return (
        "<:checkmark:465507500679233536>"
        if bool(val)
        else "<:crossmark:465507499144118272>"
    )


async def run_in_shell(args):
    # Create subprocess
    process = await asyncio.create_subprocess_shell(
        args,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE,
        # stderr=asyncio.subprocess.PIPE,
        # stdin=asyncio.subprocess.PIPE
    )
    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()
    # Return stdout
    return stdout.decode().strip()


async def get_image_attach(ctx, message):
    supported_images_files = {"png", "jpg", "jpeg"}
    if not message.attachments:
        return await ctx.send(
            embed=embeds.Embed(
                description="<:crossmark:465507499144118272> You haven't given any url or image!",
                timestamp=None,
            )
        )
    if ctx.message.attachments:
        for a in ctx.message.attachments:
            if a.filename.split(".")[1] in supported_images_files:
                if url:
                    return await ctx.send(
                        embed=embeds.Embed(
                            description="<:crossmark:465507499144118272> You have given both a url and a image! You can only provide one of those things!",
                            timestamp=None,
                        )
                    )
                url = a.proxy_url


async def command_not_found(ctx):
    try:
        await ctx.message.add_reaction("\N{BLACK QUESTION MARK ORNAMENT}")
        await asyncio.sleep(15)
        await ctx.message.remove_reaction(
            "\N{BLACK QUESTION MARK ORNAMENT}", ctx.guild.me
        )
    except (discord.Forbidden, discord.errors.NotFound):
        pass


async def get_prefix(bot, message):
    """This function will get the custom prefixes"""
    p = await bot.redis.smembers(f"guilds:{message.guild.id}:prefixes")
    return p if message.guild and p else bot.default_prefixes


async def confirm_opts(
    bot, message: discord.Message, user: discord.User, *, timeout: int = 10
) -> bool:
    """
    This will make a reaction based action confirmation screen. 
    Useful for important stuff that requires a confirmation.
    """
    emojis = {bot.custom_emojis.check: True, bot.custom_emojis.cross: False}
    for em in emojis:
        await message.add_reaction(em)

    def check(r, u):
        rchk = r.emoji in emojis
        mchk = r.message.id == message.id
        uchk = u == user
        return all((rchk, mchk, uchk))

    try:
        rct, usr = await bot.wait_for("reaction_add", check=check, timeout=timeout)
    except asyncio.TimeoutError:
        return

    return emojis[rct.emoji]


async def confirm_opts_full(
    bot, channel: discord.TextChannel, user: discord.User, *, timeout: int = 10
) -> bool:
    """
    A wrapper for the function above that does the dialog sending and closing part
    """
    dialog = await channel.send(
        embed=embeds.Embed(
            title="Are you sure?",
            description=(
                "Click on the buttons bellow to make a choice.\n"
                f"*This will timeout in {timeout}s*"
            ),
            colour=discord.Colour.gold(),
        )
    )

    if await confirm_opts(bot, dialog, user, timeout=timeout):
        await dialog.edit(
            embed=embeds.Embed(
                title="Okay then!", description=f"{bot.crossmark} **Proceeding...**"
            )
        )

        await asyncio.sleep(1)
        return True

    await dialog.edit(
        embed=embeds.Embed(
            title="Okay then!", description=f"{bot.crossmark} **Canceling...**"
        )
    )
    await asyncio.sleep(2)
    await dialog.delete()

    return False


async def discord_input(
    ctx,
    user: Union[discord.User, discord.Member],
    *,
    check: Callable = lambda _: True,
    converter: Callable = lambda m: m,
    timeout: int = 10,
):
    """Wrapper for validating user message responses using bot.wait_for"""
    # I think it's better to propagate the TimeoutError
    message = await ctx.bot.wait_for(
        "message",
        check=lambda m: all((m.author == user, m.channel == ctx.channel, check(m))),
        timeout=timeout,
    )

    if issubclass(converter, converters.Converter):
        # Let conversion errors propagate to whereever this function is being used
        proc_input = await converter().convert(ctx, message.content)
    else:
        proc_input = converter(message.content)

    return proc_input


async def closable(
    bot, messages: Tuple[discord.Message], user: discord.User, timeout: int = 120
) -> None:
    """
    This is will add a "OK" reaction to the passed message and wait until it's pressed to "close" the page.
    If it times out, the message will stay.
    This is very useful when I know there's only going to be one page, and so I don't need to call a full
    embed navigation from libneko
    """
    CLOSE_BTN = "\N{SQUARED OK}"

    original_m = messages[0]
    m = messages[1]

    try:

        def check(r, u):
            return all((r.emoji == CLOSE_BTN, r.message.id == m.id, u == user))

        await m.add_reaction(CLOSE_BTN)
        await bot.wait_for("reaction_add", check=check, timeout=timeout)

        await m.delete()
        await original_m.delete()
    except asyncio.TimeoutError:
        try:
            await m.remove_reaction(CLOSE_BTN, bot.user)
        except discord.Forbidden:
            pass
    except discord.Forbidden:
        pass


def replace_recursive(from_: str, to: str, field):
    return (
        {k: replace_recursive(from_, to, str(v)) for k, v in field.items()}
        if isinstance(field, dict)
        else ([replace_recursive(str(v), from_, to) for v in field])
        if isinstance(field, list)
        else str(field).replace(from_, to)
    )


def embed_replace(from_: str, to: str, embed: embeds.Embed) -> embeds.Embed:
    dict = {k: replace_recursive(from_, to, v) for k, v in embed.to_dict().items()}
    return discord.Embed.from_data(dict)


def with_category(name: str = None):
    def decorator(cog):
        if not name:
            _name = (
                os.path.basename(os.path.dirname(inspect.getfile(cog)))
                .replace("-", " ")
                .replace("_", " ")
                .title()
            )
        setattr(cog, "category", name if name else _name)
        return cog

    return decorator


def subtract_list(l1: list, l2: list, *, use_comp: bool = False) -> list:
    """
    Subtracts lists by either converting them to sets and subtracting them or
    by using list comprehension
    """
    if use_comp:
        return [m for m in l1 if m not in l2]

    return list(set(l1) - set(l2))


async def success_embed(ctx, message: str, invis: bool = True):
    """
    Shortcut for success messages
    """
    await ctx.send(
        embed=embeds.Embed(
            description=f"{ctx.bot.checkmark} {message}",
            colour=0x363940 if invis else discord.Colour.green(),
        )
    )


# def replacer(from_, to, object):
#     return {
#         discord.Embed: lambda e: discord.Embed.from_data(replacer(e.to_dict())),
#         dict: lambda d: {k, replacer(from_, to, v) for k, v in d.items()},
#         list: lambda t: [replacer(from_, to, st) for st in t]
#     }.get(type(object, lambda s: str(s).replace(from_, to))(object)


# 0x009933
# 0xe22020

# def with_settings():
#     def decorator():
#         ...

#     return decorator


# Unused CacheJson class. Might be useful later on
# class CachedJsonFile:
#     def __init__(self, path):
#         self.path = path
#         self.cache = None

#     async def get(self):
#         if self.cache is None:
#             await self.invalidate()
#         return self.cache

#     async def invalidate(self):
#         async with aiofiles.open(self.path) as fp:
#             self.cache = json.loads(await fp.read())

#     async def set(self, value):
#         async with aiofiles.open(path, 'w') as fp:
#             await fp.write(json.dumps(value))
#         self.cache = value


# Kek turns out discord.py has literally the same thing ;), so this's deprecated
# def find(func, iter):
#     """Finds stuff"""
#     for i in iter:
#         if func(i):
#             return i
#     return None
