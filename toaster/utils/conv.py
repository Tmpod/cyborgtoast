#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
__all__ = (
    "CommandConverter",
    "MessageConverter",
    "ColourConverter",
    "EmojiConverter",
    "GlobalUserConverter",
)

import discord
from emoji import emojize
from libneko import converters, commands
import re
from typing import Union


class CommandConverter(converters.Converter):
    """
    This converter trys to return a command (commands.Command) using the built-in method
    """

    async def convert(self, ctx, arg: str):
        cmd = ctx.bot.get_command(arg)
        if cmd is None:
            raise commands.BadArgument(f"{arg} isn't a valid command!")
        return cmd


class MessageConverter(converters.Converter):
    """
    This converter trys to return a discord.Message object from a message ID (using current chanenl)
    or by a jump url (searching with regex)
    """

    __url_pattern = re.compile(
        r"^https?://(?:canary\.)?discordapp\.com/channels/(\d+)/(\d+)/(\d+)/?$"
    )

    async def convert(self, ctx, arg: Union[int, str]):
        try:
            arg = int(arg)
        except ValueError:
            url = self.__url_pattern.findall(arg)
            if not url:
                raise commands.BadArgument(f"{arg} is a malformed message url!")
            ch_id, msg_id = map(int, url[0][-2:])
            try:
                result = await (ctx.guild.get_channel(ch_id)).get_message(msg_id)
                if result is None:
                    raise commands.BadArgument(
                        f"Message ({msg_id}) on channel {ch_id} not found!"
                    )
                return result
            except discord.Forbidden:
                raise commands.BadArgument(
                    f"Not allowed to fetch message ({msg_id}) on channel {ch_id}!"
                )
        else:
            try:
                result = await ctx.channel.get_message(arg)
            except discord.Forbidden:
                raise commands.BadArgument(
                    f"Not allowed to fetch message with {arg} ID"
                )
            if result is None:
                raise commands.BadArgument(
                    f"Message with {arg} ID in the current channel not found "
                )
            return result


class ColourConverter(converters.Converter):
    """This attemps to convert various colour notations to a discord.Colour object"""

    __acceptable = {
        n.replace("_", " "): eval(f"discord.Colour.{n}()")
        for n in {
            a
            for a in dir(discord.Colour)
            if all(  # This is quite overcomplicated but
                (  # this basically gets the colour
                    not a.startswith("_"),  # values from discord.Colour
                    callable(getattr(discord.Colour, a)),
                    a
                    not in ("r", "g", "b", "to_rgb", "from_rgb", "from_hsv", "default"),
                )
            )
        }
    }
    __acceptable["default"] = discord.Colour.gold()  # Setting the default
    __acceptable["black"] = discord.Colour(0)
    __acceptable["invis"] = discord.Colour(0x363940)

    async def convert(self, ctx, arg: Union[int, str]):
        try:
            if isinstance(arg, int):
                if arg < 16_777_215:
                    return discord.Colour(arg)
                raise commands.BadArgument(
                    f"int value for embed colour should be less than or equal to 16777215."
                )
            if arg.startswith("#"):
                arg = arg[1:]
            arg = int(arg, 16)
            return discord.Colour(arg)
        except ValueError:
            if arg not in self.__acceptable:
                raise commands.BadArgument(f"{arg} isn't a valid colour value!")
            return self.__acceptable[arg]


class EmojiConverter(converters.EmojiConverter):
    """
    This converter starts by sieving the emoji just like commands.EmojiConverter
    but then proceeds to check if it's an actual UTF-8 emoji char.
    """

    async def convert(self, ctx, arg: Union[int, str]):
        match = self._get_id_match(arg) or re.match(r"(?><a?:\w+:(\d+)>)", arg)
        result = None
        bot = ctx.bot
        guild = ctx.guild

        if match is None:
            # Try to get the emoji by name. Try local guild first.
            if guild:
                result = discord.utils.get(guild.emojis, name=arg)

            if result is None:
                result = discord.utils.get(bot.emojis, name=arg)
        else:
            emoji_id = int(match.group(1))

            # Try to look up emoji by id.
            if guild:
                result = discord.utils.get(guild.emojis, id=emoji_id)

            if result is None:
                result = discord.utils.get(bot.emojis, id=emoji_id)

        if result is None:
            result = emojize(arg, use_aliases=True)
            if result == arg:
                raise commands.BadArgument(f"Emoji {arg} wasn't found!")

        return result


class GlobalUserConverter(converters.UserConverter):
    async def convert(self, ctx, argument: Union[int, str]):
        match = self._get_id_match(argument) or re.match(r"<@!?([0-9]+)>$", argument)
        result = None
        state = ctx._state

        if match is not None:
            user_id = int(match.group(1))
            result = ctx.bot.get_user(user_id)
            if result is None:  # We also try to make a call
                result = await ctx.bot.get_user_info(user_id)
        else:
            arg = argument
            # check for discriminator if it exists
            if len(arg) > 5 and arg[-5] == "#":
                discrim = arg[-4:]
                name = arg[:-5]
                predicate = lambda u: u.name == name and u.discriminator == discrim
                result = discord.utils.find(predicate, state._users.values())
                if result is not None:
                    return result

            predicate = lambda u: u.name == arg
            result = discord.utils.find(predicate, state._users.values())

        if result is None:
            raise commands.BadArgument('User "{}" not found'.format(argument))

        return result
