#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import aioredis
import typing


RedisPool = typing.NewType("RedisPool", aioredis.Redis)
