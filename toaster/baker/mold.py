#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands, clients

from libneko.asyncinit import AsyncInit

import aiohttp
import aioredis
from asyncio import sleep as asleep
from asyncio import create_subprocess_shell
from asyncio.subprocess import PIPE as aioPIPE
from concurrent import futures
import discordlists
from datetime import timedelta
from functools import partial as fpartial
from json import loads as json_loads
from json import dumps as json_dumps
import logging
from libneko import embeds
from libneko.aggregates import Proxy
import motor.motor_asyncio as aiomotor
from os import path, listdir
from random import choice
from sys import version as sys_version
from time import monotonic
from traceback import print_exception, print_exc

from .threadweeblogger import ThreadedLoggerWebhookHandler

from toaster import __version__ as __bot_version__
from . import __version__ as __baker_version__
from toaster.utils import pretty_list, run_in_shell, get_log_num

# from toaster.baker.baker import Baker


class BreadMold(clients.Bot, AsyncInit):
    def __init__(
        self,
        *,
        client_id,
        owner_id,
        config_data: Proxy,
        baker: "toaster.baker.baker.Baker",
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.client_id = client_id
        self.owner_id = owner_id

        self.logger = logging.getLogger("cyborgtoast")
        self.logger.setLevel(logging.INFO)

        log_format = "{asctime}.{msecs:03.0f} | {levelname:^8} | {name}::{message}"
        date_format = "%Y.%m.%d %H.%M.%S"

        sh = logging.StreamHandler()
        sh.setLevel(logging.INFO)
        formatter = logging.Formatter(log_format, style="{", datefmt=date_format)
        sh.setFormatter(formatter)

        # weeb_hdlr = ThreadedLoggerWebhookHandler(
        #     config_data.logging.log_webhook, config_data.logging.weeb_name
        # )
        # self.logger.addHandler(weeb_hdlr)

        self.logger.info("Invoked!")

        self.baker = baker
        self.config = config_data
        self._token = self.config.baker.token
        self.default_prefixes = self.config.toast.default_prefixes
        self.started_at = monotonic()
        self.tpe = futures.ThreadPoolExecutor()
        self.failed_exts_on_startup = {}
        self.failed_exts = {}
        self.loc = None
        self.cmd_counter = 0

        self.logger.info("Starting...")

    async def __ainit__(
        self,
        *,
        client_id,
        owner_id,
        config_data: Proxy,
        baker: "toast.baker.baker.Baker",
        **kwargs,
    ):
        # Processing the extension list
        async with await self.baker.rsc.request("GET", "extensions.txt") as fp:
            exts_file = (await fp.read()).split("\n")
        self.default_exts = {
            f"toaster.exts.{l.strip()[1:]}" if l.startswith("*") else l.strip()
            for l in exts_file
            if l and not l.startswith("#")
        }
        ### Expanded ###
        # self.default_exts = []
        # for line in exts_file:
        #     line = line.strip()
        #     if not line or line.startswith('#'):
        #         continue
        #     if line.startswith('*'):
        #         line = 'toaster.exts.' + line[1:]
        #     self.default_exts.append(line)

        # Now loading them
        for exts in self.default_exts:
            try:
                self.load_extension(exts)
                self.logger.info(f"Loaded: {exts}")
            except BaseException as ex:
                self.logger.error(f"Could not load {exts}\n%s", ex, exc_info=True)
                self.failed_exts_on_startup[exts] = ex

        # Conntecting to redis
        if self.config.redis is not None:
            try:
                # Use the high-level interface.
                self.redis = await aioredis.create_redis_pool(**self.config.redis)
                self.logger.info(
                    f"REDIS: Created pool to {pretty_list(self.config.redis.address, sep=':')}"
                )

                @self.listen("on_logout")
                async def close_redis_pool():
                    """Close the connection pool when the bot exits."""
                    self.logger.warning("Closing the Redis connection pool...")
                    try:
                        self.redis.close()
                    except Exception as ex:
                        self.logger.error(
                            "Could not close the Redis connection!\n%s",
                            ex,
                            exc_info=True,
                        )
                        return
                    self.logger.warning("Closed the Redis connection pool!")

            except Exception as ex:
                self.logger.error(
                    "REDIS: Could not start database pool with error:\n%s\nContinuing without it",
                    ex,
                    exc_info=True,
                )
        else:
            self.logger.warning(
                "REDIS: No config was provided, so it will not be used."
            )

        if self.config.mongo is not None:
            try:
                self.mongo_client = aiomotor.AsyncIOMotorClient(
                    **self.config.mongo.address
                )
                self.logger.info(
                    f"MONGO: Created connection to {pretty_list(self.config.mongo.address, sep=':')}"
                )
                self.mongodb = self.mongo_client.main
                self.logger.info(
                    "MONGO: Got main database! Proceeding to make the test operation"
                )
                try:
                    await self.mongodb.tests.insert_one({"first_op": True})
                except Exception as ez:
                    self.logger.error(
                        f"Something went wrong! Here's the error:\n%s",
                        ex,
                        exc_info=True,
                    )

                @self.listen("on_logout")
                async def close_mongo_connection():
                    """Close the connection pool when the bot exits."""
                    self.logger.warning("Closing the MongoDB connection pool...")
                    try:
                        self.mongo_client.close()
                    except Exception as ex:
                        self.logger.error(
                            "Could not close the Mongo connection!\n%s",
                            ex,
                            exc_info=True,
                        )
                        return
                    self.logger.warning("Closed the MongoDB connection pool!")

            except Exception as ex:
                self.logger.error(
                    "MONGO: Could not start database pool with error:\n%s\nContinuing without it",
                    ex,
                    exc_info=True,
                )
        else:
            self.logger.warning(
                "MONGO: No config was provided, so it will not be used."
            )

        # Loading activity messages
        self._activities = None
        try:
            from toaster.resources.activities import ACTIVITIES

            self._activities = ACTIVITIES
        except ImportError as ex:
            self.logger.error(
                f"Could not load the activities messages with\n%s",
                ex,
                "\nProceeding with the plan-B, hard-coded message.",
                exc_info=True,
            )

        # Initial LOC counting and caching
        await self.count_loc()

    async def change_activities(self):
        await self.wait_until_ready()
        # Checking if the activities were Successfully loaded
        if not self._activities:
            self.logger.warning(
                "Could not load activities! Proceeding with the default one..."
            )
            # If not queue the default activity
            stream = discord.Streaming(
                url="https://www.twitch.tv/tmpod", name="bits of information"
            )
            await self.change_presence(activity=stream)
            return
        # Simple 'decoder'
        act_decoder = {
            "watch": lambda n: discord.Activity(
                type=discord.ActivityType.watching,
                name=n if not callable(n) else n(self),
            ),
            "play": lambda n: discord.Game(name=n if not callable(n) else n(self)),
            "listen": lambda n: discord.Activity(
                type=discord.ActivityType.listening,
                name=n if not callable(n) else n(self),
            ),
            "stream": lambda n: discord.Streaming(
                url="https://www.twitch.tv/tmpod",
                name=n if not callable(n) else n(self),
            ),
        }
        # Creating the loop
        self.logger.info(
            "Activities found and Successfully loaded! Creating the activity changer loop..."
        )
        while True:
            act_type = choice([*self._activities.keys()])
            possb = choice(self._activities[act_type])
            await self.change_presence(activity=act_decoder[act_type](possb))
            await asleep(30)

    async def dbl_poster(self):
        """
        Background stats poster for all the Discord bot list's I'm registerd in
        It reads from both a configuration file where I can easily add new platforms
        as well as from the main config file where all the API keys are stored.
        """
        if not hasattr(self.config, "dbl"):
            self.logger.info("Skipping DBL Poster task...")
            return

        self.logger.info("Starting DBL Poster task!")
        self.dbl_client = discordlists.Client(
            self
        )  # Sticking with the default interval

        for l, t in self.config.dbl.items():
            self.dbl_client.set_auth(l, t)

        await self.wait_until_ready()
        self.dbl_client.start_loop()

    async def run_in_tpe(self, func, *args, **kwargs):
        partial = fpartial(func, *args, **kwargs)
        return await self.loop.run_in_executor(self.tpe, partial)

    async def size(self) -> str:
        """
        Returns the size of the bot's dir.
        Uses a shell subprocess to execute 'du -sh'
        One-liner of:
        proc = await create_subprocess_shell("du -sh", stdout=aioPIPE, stderr=aioPIPE)
        stdout, stderr = await proc.communicate()
        size = stdout.decode[:8] # removing the tab, and the path
        return f"{size}B" # adding a B for byte
        """
        return f"{(await run_in_shell('du -sh')).split()[0]}B"

    async def count_loc(self):
        """
        Counts the lines of code.
        """
        try:
            self.logger.info("Counting and caching LOC.")
            # Runs cloc with JSON output
            raw_loc = await run_in_shell(
                "cloc --json --exclude-dir=__pycache__,venv,.idea,cache /bakery"
            )
            # Gets the number from the total line of the output for cloc
            self.loc = json_loads(raw_loc)
        finally:
            return

    def run(self):
        super().run(self._token)

    async def start(self, token: str = None, *, bot: bool = True):
        task = None
        try:
            task = self.loop.create_task(self.change_activities())
            task.add_done_callback(lambda _: task.result())
            await super().start(token or self._token, bot=bot)
        finally:
            if task is not None:
                task.cancel()

    async def logout(self):
        self.dispatch("logout")
        await super().logout()

    async def on_ready(self):
        """Boot-up finished"""

        # Setting up a creator attribute
        _creator = (await self.application_info()).owner
        self.creator = _creator

        # Setting up the logging channels
        self.logs_channel = self.get_channel(self.config.baker.channels.errors)
        self.suggestions_log = self.get_channel(self.config.toast.channels.suggestions)

        # Setting all custom emojis
        self.custom_emojis = Proxy({})
        for n, e in self.baker.sec.data.toast.emojis.items():
            self.custom_emojis[n] = self.get_emoji(e)

        # Setting some specific emojis
        self.checkmark = self.custom_emojis.check
        self.crossmark = self.custom_emojis.cross
        self.maybemark = self.custom_emojis.maybe

        # Starting the DBL poster job
        self.loop.create_task(self.dbl_poster())

        # Updating the avatar
        try:
            async with await self.baker.rsc.request(
                "GET", "avatar.png", mode="rb"
            ) as fp:
                await self.user.edit(avatar=await fp.read())
                self.logger.info("Successfully uploaded bot avatar!")
        except Exception as ex:  # If I provide a bad avatar it won't bork but it will warn me!
            self.logger.error(
                "Could not change avatar with error:\n%s", ex, exc_info=True
            )

        # Printing the ready message
        async with await self.baker.rsc.request("GET", "boot-up_message.txt") as fp:
            raw_txt = await fp.read()
        replacements = {
            "BOTVERSION": __bot_version__,
            "BAKERVERSION": __baker_version__,
            "SYSVERSION": sys_version[:6],
            "DISCORDVERSION": discord.__version__,
        }
        txt = raw_txt
        for r in replacements:
            txt = txt.replace(f"{{{r}}}", replacements[r])

        self.logger.info(txt)

        self.logger.info(
            f"Booted up with {len(self.failed_exts_on_startup)} errors while loading cogs"
        )

        # Sending the boot message
        # if path.exists(__rebootf__):
        #     async with aiofiles.open(__rebootf__, "r") as fp:
        #         reboot = await fp.readlines()
        try:
            async with await self.baker.rsc.request("GET", "REBOOT") as fp:
                reboot = await fp.readlines()
                ch = self.get_channel(int(reboot[2]))
                msg = await ch.get_message(int(reboot[3]))
                await msg.edit(
                    embed=embeds.Embed(
                        description=f"{self.checkmark} **CyborgToast restarted**",
                        colour=discord.Colour.green(),
                        timestamp=None,
                    )
                )
        except AttributeError as ex:
            self.logger.error(ex, exc_info=True)
        except discord.Forbidden as ex:
            self.logger.error(ex, exc_info=True)

        # # Sending a boot-up message
        # msg = await self.logs_channel.send(
        #     # content=self.creator.mention,
        #     embed=discord.Embed(
        #         description=f"<:checkmark:465507500679233536> **Booted-up!**\n__Errors:__ {len(self.failed_exts_on_startup) or '`No errors occurred. Hooray!`'}",
        #         color=discord.Colour.green(),
        #     ),
        # )

    @property
    def uptime(self) -> timedelta:
        """Returns the bot's uptime"""
        return timedelta(seconds=monotonic() - self.started_at)

    @property
    def client_session(self):
        try:
            cs = getattr(self, "__client_session")
        except AttributeError:
            cs = aiohttp.ClientSession()
            setattr(self, "__client_session", cs)
        finally:
            return cs
