#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

# Imports
from asyncio import set_event_loop_policy, get_event_loop
from libneko.logging import Log
import logging
from sys import argv as sys_argv

from toaster.baker.baker import Baker
from toaster.baker.threadweeblogger import ThreadedLoggerWebhookHandler
from toaster.utils.misc import get_prefix, get_log_num


class PowerTap(Log):
    def __init__(self, args=sys_argv):
        log_format = "{asctime}.{msecs:03.0f} | {levelname:^8} | {name}::{message}"
        date_format = "%Y.%m.%d %H.%M.%S"

        log_file = f"/breadvan/logs/bakery_{get_log_num()}.log"

        if len(sys_argv) == 3:
            log_lvl = sys_argv[2]
            # Setting a temporary logging level
            logging.basicConfig(
                level=log_lvl,
                filename=log_file,
                style="{",
                datefmt=date_format,
                format=log_format,
            )

        sh = logging.StreamHandler()
        sh.setLevel(logging.INFO)
        formatter = logging.Formatter(log_format, style="{", datefmt=date_format)
        sh.setFormatter(formatter)

        root_lg = logging.getLogger("")
        root_lg.addHandler(sh)

        dpy_lg = logging.getLogger("discord")
        dpy_lg.addHandler(sh)

        # fh = logging.FileHandler(
        #     log_file,
        #     mode="w",
        #     encoding="utf-8"
        #     )
        # fh.setLevel(logging.INFO)
        # fh.setFormatter(formatter)

        logger = logging.getLogger("toaster")

        logger.addHandler(sh)
        # logger.addHandler(fh)

        # Setting the event loop policy to uvloop which is faster
        logger.info("Attempting to set uvloop as the event loop policy...")
        try:
            from uvloop import EventLoopPolicy

            set_event_loop_policy(EventLoopPolicy())
            del EventLoopPolicy
        except ModuleNotFoundError:
            logger.warning("Could not load uvloop! Skipping...")
        else:
            logger.info("Using uvloop for asyncio event loop policy.")

        logger.info("TOASTER has started and is now going to check for a config file...")

        # Checking for a config file
        if len(sys_argv) < 2:
            logger.critical("No config file was passed. Cannot continue without it!")
            raise RuntimeError("Provide a config file as an argument!")

        sec_path = sys_argv[1]

        # Baker initialization
        logger.info("Starting Baker!")
        self.baker = get_event_loop().run_until_complete(
            Baker(secrets=sec_path, prefix=get_prefix)
            )

        secrets = self.baker.sec.data

        log_weeb = ThreadedLoggerWebhookHandler(
            secrets.logging.log_webhook, secrets.logging.weeb_name
            )

        logger.addHandler(log_weeb)
        root_lg.addHandler(log_weeb)
        dpy_lg.addHandler(log_weeb)
        self.baker.logger.addHandler(log_weeb)
        self.baker.bot.logger.addHandler(log_weeb)


    def run(self):
        """Starting the whole process"""
        self.baker.start()
 