#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
from asyncio import get_event_loop
import logging

from libneko.asyncinit import AsyncInit

from toaster.utils.misc import get_log_num

# from toaster.baker.update_handler import UpdateHandler
# from toaster.baker.database_selftest import DatabaseSelfTest
# from toaster.baker.module_handler import ModuleHandler
from .resource_handler import ResourceHandler
from .secrets_handler import SecretsHandler
from .error_handler import ErrorHandler

# from toaster.baker.system_assess import SysCheck

from .mold import BreadMold
from .threadweeblogger import ThreadedLoggerWebhookHandler

from discord.errors import LoginFailure


class Baker(AsyncInit):
    """
    A discord.py bot backend/engine meant to assist in bot developers in using proper
    techniques as well as give them a number of extra tools that make maintaining
    the bot easier. Based on Requiems's Cardinal (https://gitlab.com/AnakiKaiver297/Requiem-Project/tree/master/core/cardinal)
    """

    def __init__(self, secrets: str, prefix):
        """Sync init. This will just set the logger and some other stuff"""
        self.logger = logging.getLogger(__name__.lower())
        log_format = "{asctime}.{msecs:03.0f} | {levelname:^8} | {name}::{message}"
        date_format = "%Y.%m.%d %H.%M.%S"

        sh = logging.StreamHandler()
        sh.setLevel(logging.INFO)
        formatter = logging.Formatter(log_format, style="{", datefmt=date_format)
        sh.setFormatter(formatter)

        self.logger.addHandler(sh)

        self.logger.info("Invoked!")
        self.loop = get_event_loop()
        self._get_prefix = prefix

    async def __ainit__(self, secrets: str, prefix):
        """Baker initial run... performs all system startup checks as well as initializing the bot instance"""
        self.logger.info("Baker is beginning its STARTUP RUN!")
        self.rsc = ResourceHandler(self)
        self.sec = SecretsHandler(self, secrets)
        self.err = ErrorHandler(self)

        # weeb_hdlr = ThreadedLoggerWebhookHandler(
        #     self.sec.data.logging.log_webhook, self.sec.data.logging.weeb_name
        # )
        # self.logger.addHandler(weeb_hdlr)
        # await self.ModuleHandler().startall()

        ### MORE MEANINGFUL THINGS HERE IN THE FUTURE ###

        async with await self.rsc.request("GET", "bot_description.txt") as fp:
            _bot_desc = (await fp.read()).strip()

        self.bot = await BreadMold(
            client_id=436617817043763200,
            owner_id=159053372010266624,
            config_data=self.sec.data,
            # toaster_log=_logger,
            baker=self,
            command_prefix=self._get_prefix,
            description=_bot_desc,
            case_insensitive=True,
        )

        self.logger.info("Baker has finished its STARTUP RUN!")

    def start(self):
        self.logger.info("Initializing the BOT service...")
        _logger = logging.getLogger("cyborgtoast")
        while not self.sec.data.baker.token:
            _logger.critical(
                "BAKER HASN'T FOUND A TOKEN! CHECK IF THE CONFIG JSON IS CORRECT! HIT ENTER TO RETRY..."
            )
            input()
            self.sec.load()

        try:
            self.logger.info("Running the bot now!")
            self.bot.run()
        except LoginFailure:
            _logger.critical(
                "INVALID TOKEN DETECTED! CHECK IF THE CONFIG JSON IS CORRECT! HIT ENTER TO RETRY..."
            )
            input()
            self.sec.load()

    @property
    def secrets(self):
        """Returns the credentials handler"""
        return self.sec.data
