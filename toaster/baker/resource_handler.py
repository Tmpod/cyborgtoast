#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands

from libneko import aiojson
import os
import aiofp
from asyncio import shield as aioshield
from asyncio import ensure_future
from concurrent.futures import ThreadPoolExecutor
from functools import partial as fpartial

from toaster.baker import __resources__

# class _RSCContextManager:
#     """
#     This will manage the context on the 'with' statements
#     Example:
#         # rsc an instance of ResourceHandler
#         async with rsc.request("GET", "foo.txt") as foo:
#             print(foo)
#     """

#     def __init__(self, coro):
#         self._coro = coro

#     async def __aenter__(self):
#         self._obj = await self._coro
#         return self._obj

#     async def __aexit__(self, exc_type, exc, tb):
#         await self._obj.close()
#         self._obj = None


class ResourceHandler:
    """
    This handles the grabbing or posting of resources.
    It will search for a resource in the dirs specified in ./__init__.py under `__resources__`
    and handle it proply so that the interaction with resources becomes easier.
    """

    def __init__(self, baker):
        self.baker = baker
        self.set = self.post
        self._tpe = ThreadPoolExecutor()

    # def request(
    #     self,
    #     method: str,
    #     resource: str,
    #     payload=None,
    #     location: str = None,
    #     read: bool = False,
    # ):
    #     return arun(self._request(method, resource, payload, location, read))

    # def _run_in_executor(self, call, *args, **kwargs):
    #     partial = fpartial(call, *args, **kwargs)
    #     return aioshield(self.baker.loop.run_in_executor(self._tpe, partial))

    async def request(
        self,
        method: str,
        resource: str,
        *,
        payload=None,
        location: str = None,
        mode: str = "r",
    ):
        method = method.upper()
        if method not in ("GET", "POST"):
            raise TypeError("Invalid resource request method!")
        if method == "GET":
            if payload is not None:
                raise TypeError("Incompatible argument >payload< with method >GET< !")
            # return ensure_future(self._run_in_executor(self.get, resource, location), loop=self.baker.loop)
            return await self.get(resource, location, mode=mode)
        elif method == "POST":
            if location is None or payload is None:
                raise TypeError("Missing required arguments! (location or payload)")
            # return ensure_future(self._run_in_executor(self.post, resource, payload, location), loop=self.baker.loop)
            return await self.post(resource, payload, location)

    def search(self, resource: str, location: str = None):
        locations = tuple(__resources__[l] for l in __resources__)
        self.baker.logger.debug("Search request initiated!")
        for location in locations:
            self.baker.logger.debug(
                "Searching location %s for %s..." % (location, resource)
            )
            resources = {i for i in os.listdir(location)}
            self.baker.logger.debug("%s contains: %s" % (location, resources))
            if resource in resources:
                self.baker.logger.debug("Resource found! Returning path...")
                return location, resource
        self.baker.logger.error(
            f"Resource >{resource}< not found! Raising FileNotFoundError!"
        )
        raise FileNotFoundError

    async def get(
        self, resource: str, location: str = None, *, mode: str = "r"
    ):  # read: bool = False):
        try:
            self.baker.logger.debug("A resource grab has been requested!")
            location, resource = self.search(resource, location)
            # fp = aiofp.open(f"{location}{resource}") # we don't use await for the 'with' statement
            name_ext = resource.split(".")
            self.baker.logger.debug(
                "The requested%s resource >%s< was found in the directory >%s<. Returning..."
                % (f" {name_ext[1]}" if len(name_ext) > 1 else "", resource, location)
            )
            # if read:
            #     # this won't work if I use the request on a 'with' statement
            #     if resource.split(".")[1] == "json":
            #         return await aiojson.load(fp)
            #     return await fp.read()
            return aiofp.open(f"{location}{resource}", mode)
        except FileNotFoundError:
            self.baker.logger.warning(
                f"The requested resource >{resource}< was not found in any of the resource directories! Ignoring..."
            )

    async def post(self, resource: str, payload, location: str):
        if location.lower() in ("local", "cache"):
            location = __resources__["local"]
        elif location.lower() in ("global", "persistent"):
            location = __resources__["global"]
        try:
            self.baker.logger.debug("A resource post as been requested!")
            location, resource = self.search(resource)
            async with aiofp.open(f"{location}{resource}", "w") as fp:
                if resource.endswith(".json"):
                    await aiojson.dump(payload, fp)
                else:
                    await fp.write(payload)
                self.baker.logger.debug(
                    "Successfully posted >%s< in >%s<" % (resource, location)
                )
                return True
        except FileNotFoundError:
            if os.path.isdir(location) is False:
                os.mkdir(location)
            async with aiofp.open(f"{location}{resource}", "w") as fp:
                if resource.endswith(".json"):
                    await aiojson.dump(payload, fp)
                else:
                    await fp.write(payload)
                self.baker.logger.debug(
                    "Successfully posted >%s< in >%s<" % (resource, location)
                )
                return True
