#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

ACTIVITIES = {
    "play": [
        "Minesweeper",
        "Pong",
        "Tic-tac toe",
        "with the Discord API",
        "with the Hypixel API",
        "with Redis",
        "with MongoDB"
    ],
    "watch": [lambda b: f"for {b.default_prefixes[0]}help"],
    "listen": [
        "beeps and boops",
        "some smooth jazz",
        lambda b: f"{len(b.users)} users in {len(b.guilds)} servers",
    ],
    "stream": ["bits and bytes"],
}
