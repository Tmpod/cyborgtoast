#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
from toaster.utils.strings import pretty_list, titlefy

NL = "\n"

RESPS = {
    "general": [
        "Whoopsey that wasn't supposed to happen...",
        "Something's lurking in the code.",
        "Whats up Doc? Something broke...",
        "Oh I'm sorry? We're you expecting a FUNCTIONAL bot?",
        "Bots broken, Apply more espysauce to fix...",
        "Whoops that wasn't supposed to happen...",
        "How was I supposed to know pepsi and mentos don't mix?",
        "Ha Ha! Working bots, am I right?",
        "Is this the real life? Is this just brokery...?",
        "We can pretend this never happened... right?",
        "502 Bad Gateway! Wait... No that's not right...",
        "What can I say, I'm an expert at being broken",
        "<insert lame dad joke here>",
        "I'm trying to fix the problems I created when I tried to fix the problems\nI created when I tried to fix the problems I created when I...\n*Stack overflow*",
        "Its not like I meant to break... B-B-Baka",
        "Somebody call an exterminator! There are bugs in the code!",
        "Row Row Row your code gently through the web socket",
        "I have a dream that one day I will be an actual working bot!",
        "*nope.exe is now running*\nkbaithx",
        "Tried multiplying by the squareroot of the 64bit integer limit\nFalied miserably",
        "OwO Whats this\n*notices error*",
        "Uh, this is the discord bot equivalent of the blue screen of death\nSorry about that",
        "They see me breaking... they hatinn",
        "Now where did I put that error message...? Ah! There it is...",
        "Must not be enough cat girls to spin the cogs, guess I'll die now.",
        "Where them errors at? Oh! There they are...",
        "Discord having issues again? I bet they'll blame Google, or Cloudfl... Oh... I'm the borked one...",
        "Oh oh! The hoven was too hot! I'm a burnt toast now...",
        "Looks like the baker forgot an ingredient!",
    ],
    "discord.ext.commands.errors": {
        "NotOwner": "<:crossmark:465507499144118272> You are not my creator! Cya!",
        "CheckFailure": lambda e: f"<:stop:469543643124989962> You can't run that command!\n*{e.args[0]}*",  # Get the message
        "CommandOnCooldown": lambda e: f":hourglass: That command is in cooldown. Try it again in **{int(e.retry_after)}** seconds.",
        "BadArgument": lambda e: f"<:crossmark:465507499144118272> Invalid argument!\n*{pretty_list(e.args, sep='{NL}')}*",
        "BadUnionArgument": lambda e: f"<:crossmark:465507499144118272> Invalid arguments!\n*{pretty_list(e.param.name, sep='{NL}')}*",
        "MissingPermissions": lambda e: f"<:stop:469543643124989962> You don't have enough permissions to do that!\nPerms: `{pretty_list(map(titlefy, e.missing_perms))}`",
        "MissingRequiredArgument": lambda e: f"<:crossmark:465507499144118272> Missing the `{e.param.name}` argument!",
        "TooManyArguments": "You've given too many arguments for this command! Try seing the help page for more info.",
        "NoPrivateMessage": "<:crossmark:465507499144118272> This command can only be executed in a server!",
        "BotMissingPermissions": lambda e: f"<:stop:469543643124989962> I don't have enough permissions to do that!\nPerms: `{pretty_list(map(titlefy, e.missing_perms))}`",
    },
    "toaster.utils.exceptions": {
        "ModuleDisabled": ":closed_lock_with_key: This module is disabled for this server! Admins can enable it.",
        "NotAdminOrOwner": "<:stop:469543643124989962> You don't have enough permissions to do that!",
    },
    "discord.errors": {
        "Forbidden": "<:stop:469543643124989962> I'm not allowed to do that!",
        "NotFound": "<:crossmark:465507499144118272> 404 Not found... Literally",
    },
    "builtins": {
        "RuntimeError": lambda e: pretty_list(e.args, sep="\n"),
        "RuntimeWarning": lambda e: pretty_list(e.args, sep="\n"),
        "Warning": lambda e: pretty_list(e.args, sep="\n"),
        "UserWarning": lambda e: pretty_list(e.args[0], sep=""),
        "NotImplementedError": lambda e: pretty_list(e.args, sep="\n")
    },
}
