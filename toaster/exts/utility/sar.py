#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands, converters

from discord.ext.commands import Greedy, Cog
from traceback import print_exception, print_exc, format_exc
from libneko import embeds

from toaster.utils import pretty_list, with_category, subtract_list


@with_category()
class SelfRoles:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        name="selfroles",
        aliases=("sar",),
        brief="Self assignable roles 101",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def self_roles(self, ctx):
        """
        Get, remove or list roles! 
        Set or revoke the self assignable status if you have the manage roles permission!
        This provides an easy and ituitive way of getting all the available roles that you desire!
        Check the subcommands help pages for more info.
        """
        await ctx.invoke(self.list_roles)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    @self_roles.command(
        name="list", aliases=("l",), brief="Lists the available self-roles"
    )
    async def list_roles(self, ctx):
        """Lists all the registered self-obtainable roles"""
        sar = (
            await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id}, {"sar": 1})
        ).get("sar")

        if sar:  # in case there's not a document for this guild yet
            raw_roles = sar["roles"]
        else:
            raw_roles = set()

        if raw_roles:
            roles = {discord.utils.get(ctx.guild.roles, id=i) for i in raw_roles}
            nl = "\n"
            pretty_roles = f":small_blue_diamond:   {pretty_list({r.mention for r in sorted(roles, key=str)}, sep=f'{nl}:small_blue_diamond:  ')}"
        else:
            pretty_roles = "`There are no self-assignable roles for this guild yet`"
        await ctx.send(
            embed=embeds.Embed(
                title=":label: Self-obtainable roles for this guild",
                description=pretty_roles,
                timestamp=None,
            )
        )

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.bot_has_permissions(manage_roles=True)
    @self_roles.command(
        name="get", aliases=("g",), brief="Assigns the given role, if there's one"
    )
    async def get_role(self, ctx, *, role: converters.RoleConverter):
        """
        Gets you the role you specified if it exists and is available!
        You can check wich roles are available for self-assigning by doing `ds!sar list` or simply `ds!sar`
        """
        sar = (
            await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id}, {"sar": 1})
        ).get("sar")

        if not sar:  # in case there's not a document for this guild yet
            raise UserWarning(f"{self.bot.crossmark} There are no self roles yet!")

        roles = sar["roles"]

        if role.id in roles and role not in ctx.author.roles:
            await ctx.author.add_roles(role, reason="Giving self-assignable role")
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully assigned {role.mention}!",
                    timestamp=None,
                )
            )

        raise UserWarning(
            f"{self.bot.crossmark} That role is either non-existent, not self-obtainable or you already have it!"
        )

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.bot_has_permissions(manage_roles=True)
    @self_roles.command(
        name="remove", aliases=("rm",), brief="Unassigns the given role, if there's one"
    )
    async def remove_role(self, ctx, *, role: converters.RoleConverter):
        """Removes the specified role from you if you have if, of course."""
        sar = (
            await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id}, {"sar": 1})
        ).get("sar")

        if not sar:  # in case there's not a document for this guild yet
            raise UserWarning(f"{self.bot.crossmark} There are no self roles yet!")

        roles = sar["roles"]

        if role.id in roles and role in ctx.author.roles:
            await ctx.author.remove_roles(role, reason="Removing self-assignable role")
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully unassigned {role.mention}!",
                    timestamp=None,
                )
            )

        raise UserWarning(
            f"{self.bot.crossmark} You either don't have that role or it's not self-obtainable!"
        )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @commands.has_permissions(manage_roles=True)
    @self_roles.command(
        name="set", aliases=("s",), brief="Sets an existing role as self-obtainable"
    )
    async def set_self_role(self, ctx, roles: Greedy[converters.RoleConverter]):
        """Registers a role as self-assignable. This will make other people able to get it by using the `sar get` command."""
        sar = (
            await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id}, {"sar": 1})
        ).get("sar")

        if not sar:  # in case there's not a document for this guild yet
            await self.bot.mongodb.guilds.insert_one(
                {"_id": ctx.guild.id, "sar.roles": [r.id for r in roles]}
            )

        else:
            reg_roles = sar["roles"]

            bot_top_role = ctx.guild.roles.index(ctx.guild.me.roles[-1])
            usr_top_role = ctx.guild.roles.index(ctx.author.roles[-1])
            for r in roles:
                if r.id in reg_roles:
                    raise UserWarning(
                        f"{self.bot.crossmark} {r.mention} is already self-obtainable!"
                    )
                if ctx.guild.roles.index(r) > usr_top_role:
                    raise UserWarning(
                        f"{self.bot.crossmark} {r.mention} is above your  highest role, so I'm not able to register it!"
                    )

                if ctx.guild.roles.index(r) > bot_top_role:
                    raise UserWarning(
                        f"{self.bot.crossmark} {r.mention} is above my highest role, so I'm not able to register it!"
                    )

            await self.bot.mongodb.guilds.update_one(
                {"_id": ctx.guild.id},
                {"$addToSet": {"sar.roles": {"$each": [r.id for r in roles]}}},
            )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully set {pretty_list((r.mention for r in roles), sep=', ')} as a self-obtainable role!",
                timestamp=None,
            )
        )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @commands.has_permissions(manage_roles=True)
    @self_roles.command(
        name="revoke",
        aliases=("rv",),
        brief="Revokes the self-role qualification from a role",
    )
    async def revoke_self_role(self, ctx, roles: Greedy[converters.RoleConverter]):
        """Unregisters a self-obtainable role from the list."""
        sar = (
            await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id}, {"sar": 1})
        ).get("sar")

        if not sar:  # in case there's not a document for this guild yet
            await self.bot.mongodb.guilds.update_one(
                {"_id": ctx.guild.id, "sar.roles": []}
            )

            raise UserWarning(
                f"{self.bot.crossmark} There are no registered roles yet."
            )

        reg_roles = sar["roles"]

        if not reg_roles:
            raise UserWarning(f"{self.bot.crossmark} There are no registered roles.")

        for r in roles:
            if r.id not in reg_roles:
                raise UserWarning(
                    f"{self.bot.crossmark} {r.mention} is not self-obtainable!"
                )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id},
            {"$pull": {"sar.roles": {"$in": [r.id for r in roles]}}},
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully revoked the self-role tag from {pretty_list((r.mention for r in roles), sep=', ')}",
                timestamp=None,
            )
        )

    # @commands.has_permissions(manage_roles=True)
    # @self_roles.command(
    #     name="create",
    #     aliases=['c'],                                        # IDK IF IM GONNA CONTINUE THIS COMMAND, CUS IT SEEMS A BIT UNECESSARY
    #     brief="Creates a fresh new self-assignable role")
    # async def create_self_role(self, ctx, name: str, hoist: bool, pingable: bool):
    #     try:
    #         await ctx.guild.create_role(name=name, hoist=)


def setup(bot):
    bot.add_cog(SelfRoles(bot))
