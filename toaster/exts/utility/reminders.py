#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import discord
from discord.ext.commands import Cog
from libneko import commands

from toaster.utils import with_category, closable

@with_category()
class Reminders:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="remind", aliases=("rmd",), brief="Reminders for your forgetful needs")
    async def remind_cmd(self, ctx):
        ...

def setup(bot):
    bot.add_cog(Reminders(bot))
    