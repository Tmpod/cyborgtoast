#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands, converters, embeds

from toaster.utils import pretty_list, with_category, subtract_list, MessageConverter


@with_category()
class SelfRoleMenu:
	"""
	Setup a message with reactions for people to get roles
	"""
    def __init__(self, bot):
        self.bot = bot

    @commands.group(name="rolemenu", aliases=("rlm", "sarm"), brief="Self-assignable roles in a fancy menu!", case_insensitive=True, invoke_without_command=True)
    async def reaction_sar(self, ctx):
        ...

    @commands.group(name="rolemenu", aliases=("rlm", "sarm"), brief="Self-assignable roles in a fancy menu!", case_insensitive=True, invoke_without_command=True)
    async def reaction_sar(self, ctx):
        ...




 def setup(bot):
	bot.add_cog(SelfRolesMenu(bot))