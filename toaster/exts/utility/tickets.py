#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import discord
from discord.ext.commands import Cog
from libneko import commands

from asyncio import TimeoutError as AIOTimeoutError
from datetime import datetime
from libneko import embeds, converters
from typing import Set
import re
from secrets import token_hex
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from toaster.utils import with_category, discord_input, confirm_opts_full, chk_or_crs

DEFAULT_MAX_GLOBAL_COUNT = 15

DEFAULT_MAX_PER_USER_COUNT = 2

MAX_GLOBAL_COUNT = 40

MAX_USER_COUNT = 10

MAX_TOPIC_LENGHT = 500

MAX_REASON_LENGHT = 200

RUNNING_SETUPS = []
# AUTHOR_ID_PATTERN = re.compile(r"^(.{2,32})#(\d{4}) \(ID: (\d{17,19})\)$")


def is_setup_already_running():
    def decorator(ctx):
        if ctx.guild.id in RUNNING_SETUPS:
            return False
        return True

    return commands.check(decorator)


@with_category()
class Tickets:
    STATES = {True: "ON", False: "OFF"}

    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    def _get_ticket_info(ticket: discord.TextChannel):
        raw_info = ticket.topic
        if raw_info is None:
            return

        info = yaml.load(raw_info, Loader=Loader)

        if not isinstance(info, dict):
            return

        return info

    def _get_user_tickets(
        self, user: discord.Member, channels: Set[discord.TextChannel]
    ):
        tickets = set()
        for c in channels:
            t_data = self._get_ticket_info(c)
            if t_data is None or not isinstance(t_data, dict):
                continue

            if t_data.get("Author ID") is not None:
                tickets.add(c)

        return tickets

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @commands.group(
        name="ticket",
        aliases=("tickets", "tk"),
        brief="Tickets 101",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def tickets_group(self, ctx, *, topic: str):
        """
        Easily create, solve and close tickets! Great for any kind of support team!
        They are super customizable and easy to use. 
        To setup just do `<p>ticket settings setup` and you're good to go! Create a ticket with the `open` subcommand
        or just by doing `ticket <topic>`. To close it, you just got to use `close` subcommand and provide a reason.
        The settings can be tweaked at any time with with the commands under the `settings` subgroup! 
        """
        await ctx.invoke(self.create_ticket, topic=topic)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @tickets_group.command(
        name="open", aliases=("o", "new", "n"), brief="Create a new ticket"
    )
    async def create_ticket(self, ctx, *, topic: str = "General support"):
        """
        Opens a new ticket with the given topic.
        Note that you'll only be allowed to open a ticket if you haven't surpassed the opened tickets limit per user
        or if there aren't too many tickets opened serverwide yet.

        All info on the ticket will be stored on the channel topic and it's adisable you do not change it unless you
        know what you are doing (the channel topic must always be valid YAML and contain at least the default fields).
        """
        if len(topic) > MAX_TOPIC_LENGHT:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} That topic is too big! "
                    "It must be `{MAX_TOPIC_LENGHT}` characters long at max."
                )
            )

        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        if not tickets_st["state"]:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The Tickets module is currently turned **OFF**, "
                    "so you can't open any tickets.\n"
                    "Try again later or contact the staff if you think this is a mistake."
                )
            )

        t_catg = ctx.guild.get_channel(tickets_st["category"])
        if t_catg is None:
            raise UserWarning(
                (
                    f"{self.bot.maybemark} It seems that the registered tickets category doesn't exist anymore.\n"
                    "Please, ask the staff to register another category before trying again."
                )
            )

        support_role = ctx.guild.get_role(tickets_st["role"])
        if support_role is None:
            raise UserWarning(
                (
                    f"{self.bot.maybemark} It seems that the registered support team role doesn't exist anymore.\n"
                    "Please, ask the staff to register another role before trying again."
                )
            )

        opened_tickets = {
            c for c in t_catg.channels if isinstance(c, discord.TextChannel)
        }

        if len(opened_tickets) >= tickets_st["globalcount"]:
            raise UserWarning(
                f"{self.bot.crossmark} There're already too many tickets opened! Try again later..."
            )

        user_tickets = self._get_user_tickets(ctx.author, opened_tickets)
        if len(user_tickets) > tickets_st["usercount"]:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} You already have too many tickets opened. "
                    "Close some first and try again later."
                )
            )

        perm_overwrites = {
            ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False),
            ctx.guild.me: discord.PermissionOverwrite(
                read_messages=True, send_messages=True
            ),
            support_role: discord.PermissionOverwrite(
                read_messages=True, send_messages=True, manage_messages=True
            ),
        }

        ticket_id = token_hex(3)

        ticket = await ctx.guild.create_text_channel(
            f"ticket-{ticket_id}", overwrites=perm_overwrites, category=t_catg
        )

        ticket_info = yaml.dump(
            {
                "Author": f"{ctx.author}",
                "Author ID": ctx.author.id,
                "Topic": topic,
                "Ticket ID": ticket_id,
                "Created on": datetime.utcnow().strftime("%x at %X (UTC)"),
            },
            default_flow_style=False,
        )

        await ticket.edit(topic=ticket_info)  # Can't do this directly on creation...

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully created your ticket! Check it out here: {ticket.mention}"
            )
        )

        await ticket.send(
            content=support_role.mention,
            embed=embeds.Embed(
                description=":tickets: New ticket! Check the channel topic for more info."
            ),
        )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @tickets_group.command(
        name="close", aliases=("cl", "delete", "d"), brief="Close a ticket"
    )
    async def close_ticket(self, ctx, *, reason: str):
        """
        Closes the current ticket with the given reason, as well as sending a ticket report to the ticket author.
        This can only be done on a ticket channel under the registered ticket channel category, and only the ticket
        author or a member of the support team can close the ticket.
        When you do so, you'll be presented with a confirmation dialog just to prevent accidental closings.
        """
        if len(reason) > MAX_REASON_LENGHT:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} That reason is too big! "
                    f"It must be `{MAX_REASON_LENGHT}` characters long at max."
                )
            )

        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        if ctx.channel.category.id != tickets_st["category"]:
            raise UserWarning(
                f"{self.bot.crossmark} You cannot use this outside the tickets category!"
            )

        ticket_info = self._get_ticket_info(ctx.channel)

        if any(
            (
                ticket_info is None,
                ticket_info.get("Author ID") is None,
                ticket_info.get("Ticket ID") is None,
            )
        ):
            raise UserWarning(
                (
                    f"{self.bot.crossmark} You cannot use this outside a ticket channel! "
                    "If you are convinced this is a ticket channel, someone has messed around with it's topic and so I can no longer check the needed information to make a safe close."
                )
            )

        support_role = ctx.guild.get_role(tickets_st["role"])
        if support_role is None:
            raise UserWarning(
                (
                    f"{self.bot.maybemark} It seems that the registered support team role doesn't exist anymore.\n"
                    "Please, ask the staff to register another role before trying again."
                )
            )

        if (
            ticket_info["Author ID"] != str(ctx.author.id)
            and support_role not in ctx.author.roles
        ):
            raise UserWarning(
                f"{self.bot.crossmark} You are not the ticket's author not a member of the support team, and so you cannot close this ticket!"
            )

        if await confirm_opts_full(self.bot, ctx.channel, ctx.author):
            await ctx.channel.delete()
        else:
            return

        ticket_author = ctx.guild.get_member(int(ticket_info["Author ID"]))

        emb = embeds.Embed(
            title=f":tickets: Ticket Closed",
            description=f"Ticket `{ticket_info['Ticket ID']}` has been closed, so here you have a brief report about it.",
        )
        emb.add_field(name="Topic", value=ticket_info["Topic"])
        emb.add_field(name="Opened on", value=ticket_info["Created on"], inline=True)
        emb.add_field(
            name="Closed by",
            value=ctx.author if ctx.author != ticket_author else "You",
            inline=True,
        )
        emb.add_field(name="Reason", value=reason)

        try:
            await ticket_author.send(embed=emb)
        except:
            pass

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @tickets_group.group(
        name="settings",
        aliases=("st",),
        brief="Manage the ticket settings",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def ticket_settings(self, ctx):
        """
        Setup or tweak the Tickets feature for this guild!
        The available settings are:
            • `Support Team role` (the people to which the channel will be unlocked apart from the ticket author)
            • `Ticket Category` (the channel category under which the ticket channels will be created)
            • `Max Global Ticket Count` (the max number of tickets that can be opened at the same time serverwide)
            • `Max User Ticket Count` (the max number of tickets a user can have opened at any given time)

        You can also use the `toggle` subcommand to turn On or Off this module.
        """
        await ctx.invoke(self.settings_overview)

    @ticket_settings.command(
        name="overview",
        aliases=("ov",),
        brief="Get an overview of the current settings",
    )
    async def settings_overview(self, ctx):
        """
        Displays a little board with the current registered settings for this module.
        To tweak them, you can use any of the other subcommands.
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )
        emb = embeds.Embed(
            title="Tickets Settings",
            description="Here're the current settings for the ticket feature",
        )
        emb.add_field(
            name="State",
            value=chk_or_crs(tickets_st["state"]),
            inline=False,
        )
        emb.add_field(
            name="Support Role",
            value=ctx.guild.get_role(tickets_st["role"]).mention,
            inline=False,
        )
        emb.add_field(
            name="Tickets Channel Category",
            value=ctx.guild.get_channel(tickets_st["category"]).mention,
            inline=False,
        )
        emb.add_field(
            name="Max Opened Tickets",
            value=(
                f"**Global:** `{tickets_st.get('gobalcount') or DEFAULT_MAX_GLOBAL_COUNT}`\n"
                f"**Per User:** `{tickets_st.get('usercount') or DEFAULT_MAX_PER_USER_COUNT}`"
            ),
            inline=False,
        )

        await ctx.send(embed=emb)

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.guild)
    @is_setup_already_running()
    @ticket_settings.command(name="setup", aliases=("sp",), brief="Interactive setup")
    async def setup_tickets(self, ctx):
        """
        Initial interactive setup session. This will start up a interactive setup session which will register the first
        settings by asking some questions to which you have to answer.
        This can only be done once and only one session can be run at a time. The session will fail if you take too
        long to give a response or if you give an invalid response.
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if tickets_st:
            raise UserWarning(
                "The Tickets have already been setup! "
                "You can always tweak the settings using the other subcommands.\n"
                "Check the help page for more info"
            )

        RUNNING_SETUPS.append(ctx.guild.id)

        async def end_session(reason: str):
            """Permaturely ends the session"""
            emb.description = f"{reason}\nPlease try again later..."
            emb.colour = discord.Colour.red()
            await dialog.edit(embed=emb)
            RUNNING_SETUPS.remove(ctx.guild.id)

        async with ctx.typing():
            emb = embeds.Embed(
                title="Interactive setup session for the Tickets settings",
                description=(
                    "Welcome to this interactive setup!\n"
                    "I'm going to start asking for some stuff and all you need to do is answer accoordingly. "
                    "This message will be edited as we proceed in the setup so always keep at sight.\n"
                    "*Answer with `OK` to proceed!*"
                ),
            )
            dialog = await ctx.send(embed=emb)

            try:
                usr_input = await discord_input(
                    ctx, ctx.author, converter=converters.BoolConverter, timeout=20
                )
            except (commands.BadArgument, commands.ConversionError):
                return await end_session(
                    "Interactive setup session canceled due to invalid response!"
                )
            except AIOTimeoutError:
                return await end_session(
                    "You took to long, so the interactive setup session has been canceled!"
                )

            if not usr_input:
                emb.description = "Alright! Canceled the interactive setup!"
                emb.colour = discord.Colour.red()
                return await dialog.edit(embed=emb)

            tickets_st = {"state": True}

            # Role
            emb.description = (
                "Alright, proceeding...\n"
                "So, the first thing I need is the Support Team role. "
                "This role will be used to give access to opened tickets to users that have it. "
                "Just give its name, ID, or mention and I'll try my best to find it!\n"
                "*Waiting...*"
            )
            await dialog.edit(embed=emb)
            try:
                usr_input = await discord_input(
                    ctx, ctx.author, converter=converters.RoleConverter, timeout=20
                )
                tickets_st["role"] = usr_input.id
            except (commands.BadArgument, commands.ConversionError):
                return await end_session(
                    "Interactive setup session canceled due to invalid role!"
                )
            except AIOTimeoutError:
                return await end_session(
                    "You took to long, so the interactive setup session has been canceled!"
                )
            # Category
            emb.description = (
                f"Very well! I've just registered {usr_input.mention} as the Support Team role!\n"
                "Now I need the channel category in which the tickets will be created and managed! "
                "You can either give its name or ID.\n"
                "*Waiting...*"
            )
            await dialog.edit(embed=emb)
            try:
                usr_input = await discord_input(
                    ctx,
                    ctx.author,
                    converter=converters.InsensitiveCategoryConverter,
                    timeout=20,
                )
                tickets_st["category"] = usr_input.id
            except (commands.BadArgument, commands.ConversionError):
                return await end_session(
                    "Interactive setup session canceled due to invalid channel category!"
                )
            except AIOTimeoutError:
                return await end_session(
                    "You took to long, so the interactive setup session has been canceled!"
                )

            # Global Count
            emb.description = (
                f"Good! I've registered **__{usr_input.mention}__** as the Tickets Category!\n"
                "Next up is the max global ticket count. "
                "This is the maximum tickets that can be opened at the same time serverwide! "
                f"Just pass any digit ranging from **1** to **{MAX_GLOBAL_COUNT}**\n"
                "*Waiting...*"
            )
            await dialog.edit(embed=emb)
            try:
                usr_input = await discord_input(
                    ctx, ctx.author, converter=converters.IntConverter, timeout=20
                )
                tickets_st["globalcount"] = usr_input
            except (commands.BadArgument, commands.ConversionError):
                return await end_session(
                    "Interactive setup session canceled due to invalid integer!"
                )
            except AIOTimeoutError:
                return await end_session(
                    "You took to long, so the interactive setup session has been canceled!"
                )

            # User Count
            emb.description = (
                f"Good! I've registered **`{usr_input}`** as the max global ticket count!\n"
                "Next up is the max user ticket count. "
                "This is the maximum tickets that can be opened at the same time by some user! "
                f"Just pass any digit ranging from **1** to **{MAX_USER_COUNT}**"
                "*Waiting...*"
            )
            await dialog.edit(embed=emb)
            try:
                usr_input = await discord_input(
                    ctx, ctx.author, converter=converters.IntConverter, timeout=20
                )
                tickets_st["usercount"] = usr_input
            except (commands.BadArgument, commands.ConversionError):
                return await end_session(
                    "Interactive setup session canceled due to invalid integer!"
                )
            except AIOTimeoutError:
                return await end_session(
                    "You took to long, so the interactive setup session has been canceled!"
                )

            await self.bot.mongodb.guilds.update_one(
                {"_id": ctx.guild.id}, {"$set": {"tickets": tickets_st}}
            )

            emb.description = (
                f"Ok! Registered **`{usr_input}`** as the max user ticket count. We are all done now!\n"
                "The Tickets have been setup and can start being used! "
                f"You can always tweak any setting with the commands from the "
                f"`{ctx.prefix}tickets setttings` command group!\n"
                "I hope you have good time with this service and if you ever run across any issues "
                f"just join my support server (get the link with `{ctx.prefix}support`)"
            )
            emb.colour = discord.Colour.green()

            await dialog.edit(embed=emb)

            RUNNING_SETUPS.remove(ctx.guild.id)

    @ticket_settings.command(
        name="toggle", aliases=("t",), brief="Toggles this module On or Off"
    )
    async def toggle_tickets(self, ctx):
        """
        Flick the switch for the Tickets feature. This will turn the module On if it's Off or Off if it's On.
        If it's turned Off no more tickets can be opened, even though already existing tickets can be closed.
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        new = not tickets_st["state"]

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"state": new}}
        )

        await ctx.send(
            embed=embeds.Embeds(
                description=f"{self.bot.checkmark} Successfully toggled the Tickets **{self.STATES[new]}**!"
            )
        )

    @ticket_settings.command(
        name="role", aliases=("r",), brief="Setup the support role"
    )
    async def settings_role(self, ctx, role: discord.Role):
        """
        Change the Support Team role. This is the role that gives access to opened tickets and is the role that gets
        pinged when a new ticket is opened. It also gives a user the ability to close a ticket.
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        if role.id == tickets_st["role"]:
            raise UserWarning(
                f"{self.bot.maybemark} That is already the registered support role!",
                "gold",
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"tickets.role": role.id}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully updated support role to {role.mention}!"
            )
        )

    @ticket_settings.command(
        name="category", aliases=("c",), brief="Setup the tickets category"
    )
    async def settings_category(self, ctx, category: discord.CategoryChannel):
        """
        Change the Tickets channel category. This will be the category under which the tickets will be opened.
        The `ticket close` command can only be executed on that category.
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        if category.id == tickets_st["category"]:
            raise UserWarning(
                f"{self.bot.maybemark} That is already the registered ticket category!",
                "gold",
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"tickets.category": category.id}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully updated ticket category to **{category.mention}**!"
            )
        )

    @ticket_settings.command(
        name="globalcount",
        aliases=("gc",),
        brief="Set the max opened tickets at one time",
    )
    async def max_ticket_count(self, ctx, count: int):
        """
        This is the maximum severwide number of opened tickets. If this cap is reached no more tickets can be created.

        For now, this count can go as high as 40 tickets (this may be subject to change if I get a better host)
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        if count == tickets_st["category"]:
            raise UserWarning(
                f"{self.bot.maybemark} That is already the registered max global count!",
                "gold",
            )

        if count <= 0:
            raise UserWarning(
                f"{self.bot.crossmark} That is too low! It must be at least `1`."
            )

        if count > MAX_GLOBAL_COUNT:
            raise UserWarning(
                f"{self.bot.crossmark} That is too high! It must be at max `{MAX_GLOBAL_COUNT}`."
            )

        changes = {"$set": {"tickets.globalcount": count}}

        maybe_more = ""
        if count < tickets_st["usercount"]:
            # We are gonna do a ratio from the old values and keeping it for the new ones
            ratio = tickets_st["usercount"] / tickets_st["globalcount"]
            new_userc = round(ratio * count)
            changes["$set"]["tickets.usercount"] = new_userc
            maybe_more = f"\n*Also changed the **per-user** count to `{new_userc}` as otherwise it would be bigger than the new global count.*"

        await self.bot.mongodb.guilds.update_one({"_id": ctx.guild.id}, changes)

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully updated max global count to {count}!{maybe_more}"
            )
        )

    @ticket_settings.command(
        name="usercount", aliases=("uc",), brief="Set the max opened tickets per user"
    )
    async def tickets_per_user(self, ctx, count: int):
        """
        This is the maximum number of opened tickets in a per user basis. If this cap is reached by a user, they won't
        be able to open any more tickets.

        For now, this count can go as high as 10 tickets (this may be subject to change if I get a better host)
        """
        tickets_st = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"tickets": 1}  # just return the ticket settings
            )
        ).get("tickets")

        if not tickets_st:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} The ticket feature hasn't been setup yet!\n"
                    f"Use `{ctx.prefix}tk st setup` to start an interactive setup."
                )
            )

        if count == tickets_st["category"]:
            raise UserWarning(
                f"{self.bot.maybemark} That is already the registered max per-user count!",
                "gold",
            )

        if count <= 0:
            raise UserWarning(
                f"{self.bot.crossmark} That is too low! It must be at least `1`."
            )

        if count > MAX_USER_COUNT:
            raise UserWarning(
                f"{self.bot.crossmark} That is too high! It must be at max `{MAX_USER_COUNT}`."
            )

        if count > tickets_st["globalcount"]:
            raise UserWarning(
                f"{self.bot.crossmark} That is higher than the current max global count! (`{tickets_st['globalcount']}`)"
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"tickets.usercount": count}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully updated max per-user count to {count}!"
            )
        )


def setup(bot):
    bot.add_cog(Tickets(bot))
