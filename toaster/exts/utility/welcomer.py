#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This is a cool feature-filled welcomer 
"""

import discord
from discord.ext.commands import Cog
from libneko import commands
from libneko import embeds
from traceback import print_exc
from textwrap import dedent
from asyncio import sleep as asleep
from datetime import datetime, timedelta
from toaster.utils import chk_or_crs, with_category, to_lower
from random import choice

from typing import Union, Tuple, Text


@with_category()
class Welcomer:
    """
    This is a feature-filled welcomer. Customisable, beautiful and info-packed!
    """

    GREEN_HUE = 0x51E723  # Nice green hue
    RED_HUE = 0xFB3719  # Nice red/orange
    STATES = {True: "ON", False: "OFF"}
    MAX_MSG_LEN = 100
    VALID_STYLES = ("detailed", "d", "simple", "s")
    STYLE_ALIASES = {"d": "detailed", "s": "simple"}
    DEFAULT_STYLE = "simple"

    def __init__(self, bot):
        self.bot = bot

    async def get_welcomer_bits(
        self, msg_type: str, member: discord.Member
    ) -> Union[Tuple[None, None, None], Tuple[int, Text, Text]]:
        """This will get the channel and message that corresponds to the given member"""
        try:
            welcomer = (
                await self.bot.mongodb.guilds.find_one(
                    {"_id": member.guild.id}, {"welcomer": 1}
                )
            ).get("welcomer")
            if not welcomer:
                return None, None, None
            state = welcomer["state"]
            channel_id = welcomer["channel"]
            if state:  # Checking if the welcomer is active or not
                channel = (
                    member.guild.get_channel(channel_id) or member.guild.system_channel
                )
            else:
                return None, None, None
            raw_msg = (
                welcomer[msg_type] or f"{msg_type.title()} **{member.display_name}**!"
            )
            message = (
                raw_msg.replace(r"%%", member.guild.name)
                .replace("&&", str(member))
                .replace("@@", member.mention)
                .replace("$$", member.display_name)
            )
        except Exception as ex:
            self.bot.logger.error(ex, exc_info=True)
            message = f"{msg_type.title()} **{member.display_name}**!"

        return channel, message, welcomer.get("style", self.DEFAULT_STYLE)

    @staticmethod
    def pretty_timespan(timespan: timedelta) -> str:
        """
        This returns a pretty string of a timedelta object
        This method was made by Espy in the Dev-Sebi project.
        """
        total = int(timespan.total_seconds())

        mins, secs = divmod(total, 60)
        hours, mins = divmod(mins, 60)
        days, hours = divmod(hours, 24)
        months, days = divmod(days, 30)
        years, months = divmod(months, 12)

        bits = []
        years and bits.append(f'{years} year{years - 1 and "s" or ""}')
        months and bits.append(f'{months} month{months - 1 and "s" or ""}')
        days and bits.append(f'{days} day{days - 1 and "s" or ""}')
        hours and bits.append(f'{hours} hour{hours - 1 and "s" or ""}')
        mins and bits.append(f'{mins} minute{mins - 1 and "s" or ""}')
        secs and bits.append(f'{secs} second{secs - 1 and "s" or ""}')

        return ", ".join(bits)

    async def on_member_join(self, member: discord.Member):
        """Handles a new user join"""
        ch, msg, style = await self.get_welcomer_bits("greetings", member)

        if ch:
            if style == "simple":
                emb = embeds.Embed(description=msg, colour=self.GREEN_HUE)

            else:
                emb = embeds.Embed(
                    title=f"{'**BOT** ' if member.bot else ''}{member} joined!",
                    description=dedent(
                        f"""
                        {msg}

                        **ID**: {member.id}
                        **Mention**: {member.mention}
                        **Account created on**: {member.created_at.strftime(r"%x at %X")}
                        """
                    ),
                    colour=self.GREEN_HUE,
                )

                emb.set_footer(
                    text=f"Member #{len(member.guild.members)} (of which {sum(m.bot for m in member.guild.members)} are bots)",
                    icon_url=member.guild.icon_url,
                )
                emb.set_thumbnail(url=member.avatar_url)

            await ch.send(embed=emb)

    async def on_member_remove(self, member: discord.Member):
        """Handles the nerfing of a member"""
        ch, msg, style = await self.get_welcomer_bits("farewell", member)

        if ch:
            if style == "simple":
                emb = embeds.Embed(description=msg, colour=self.RED_HUE)

            else:
                await asleep(4)

                reason_type = "left"
                reason = None

                # Check if banned or kicked
                try:
                    async for entry in member.guild.audit_logs():
                        created_at = entry.created_at

                        if created_at < datetime.utcnow() - timedelta(seconds=7.5):
                            break

                        # target could be any object, it may not have an ID. (Cite: the docs)
                        elif (
                            entry.target is not None
                            and getattr(entry.target, "id", -1) == member.id
                        ):
                            action = entry.action

                            if action is discord.AuditLogAction.ban:
                                reason_type = "ban"
                                reason = entry.reason
                                break
                            elif action is discord.AuditLogAction.kick:
                                reason_type = "kick"
                                reason = entry.reason
                                break

                except discord.Forbidden:
                    pass

                member_str = (
                    f"{member}{f'(a.k.a {member.nick})' if member.nick else ''}"
                )

                if reason_type == "left":
                    formatted_msg = f"{member_str} just left!"
                elif reason_type == "kick":
                    formatted_msg = f"{member_str} was kicked!"
                elif reason_type == "ban":
                    formatted_msg = f"{member_str} was banned!"
                else:
                    assert False, f"Unrecognised reason_type {reason_type}"

                emb = embeds.Embed(
                    title=formatted_msg,
                    colour=self.RED_HUE,
                    description=dedent(
                        f"""
                        {msg}
                        %%REASON%%
                        **Top role**: {member.top_role.mention if str(member.top_role) != '@everyone' else member.top_role}
                        **Mention**: {member.mention}
                        **ID**: {member.id}
                        **Account created at**: {member.created_at.strftime(r"%x at %X")}
                        **Joined here at**: {member.joined_at.strftime(r"%x at %X")}
                        **Was here for**: {self.pretty_timespan(datetime.utcnow() - member.joined_at)}
                        """
                    ),
                )

                if reason is not None:
                    emb.description = emb.description.replace(
                        r"%%REASON%%", f"\n**Reason:** {reason}\n"
                    )
                else:
                    emb.description = emb.description.replace(r"%%REASON%%", "")

                emb.set_author(
                    name=f"{'**BOT** ' if member.bot else ''}{member} left!",
                    icon_url=member.guild.icon_url,
                )
                emb.set_footer(
                    text=f"{len(member.guild.members)} members remain (of which {sum(m.bot for m in member.guild.members)} are bots)",
                    icon_url=member.guild.icon_url,
                )
                emb.set_thumbnail(url=member.avatar_url)

            await ch.send(embed=emb)

    @commands.group(
        name="welcomer",
        aliases=("wc",),
        brief="Settings for the Welcomer feature",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def welcomer_group(self, ctx):
        """This group holds the settings for the welcomer feature!"""
        await ctx.invoke(self.welcomer_overview)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @welcomer_group.command(
        name="overview",
        aliases=("ov",),
        brief="Gives an overview of the current settings",
    )
    async def welcomer_overview(self, ctx):
        """
        Shows an overview of the current registered settings for the Welcomer.
        It will show: 
            • State (if the welcomer is turned on or off)
            • Channel (where the messages will be sent to)
            • Greeting (the message that will be displayed when a new user joins)
            • Farewell (the message that will be displayed when a member leaves)

        __Note:__ Both the greeting and farewell will be displayed as the raw message, 
        meaning that it will have the replacement symbols. If you want to learn more
        about those, check the `preview`, `greeting` or `farewell` commands.
        You can also use the first one to get a accurate preview of what the messages
        will look like. 
        """
        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer:
            state = welcomer["state"]
            ch = discord.utils.get(ctx.guild.channels, id=welcomer["channel"])
            footer = "These are the raw messages, check the preview command description to know more about these."
            g_msg = welcomer["greetings"]
            f_msg = welcomer["farewell"]
        else:
            footer = "The welcomer hasn't been setup yet. Use the channel command (greeting and farewell too if you want) then toggle it on to get it working."
            state = False

        emb = embeds.Embed(
            title="Welcomer overview",
            description="Here're the current settings for the welcomer!",
        )
        emb.add_field(name="State", value=chk_or_crs(state), inline=True)
        if welcomer:
            emb.add_field(name=":card_index: Channel", value=ch.mention, inline=True)
            emb.add_field(
                name=":barber: Style",
                value=f"`{welcomer['style'].title()}`",
                inline=True,
            )
            emb.add_field(
                name=":inbox_tray: Greeting message",
                value=g_msg or f"*`Default`* Greetings **&&**!",
            )
            emb.add_field(
                name=":outbox_tray: Farewell message",
                value=f_msg or f"*`Default`* Farewell **&&**!",
            )
        emb.set_footer(text=footer)

        await ctx.send(embed=emb)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @welcomer_group.command(name="toggle", aliases=("t",), brief="Toggles the Welcomer")
    async def toggle_welcomer(self, ctx):
        """
        Turns the Welcomer on or off depending on the current state.
        You can check out the current state wth the `overview` command.
        """
        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer is None:
            raise UserWarning(
                f"{self.bot.crossmark} Cannot toggle the Welcomer as it hasn't been setup yet!\n"
                "Check out the `channel` command to start!"
            )
        new_state = not welcomer["state"]  # inverting the current state
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"welcomer.state": new_state}}
        )
        await ctx.send(
            embed=embeds.Embed(
                description=f"{chk_or_crs(new_state)} Turned the welcomer **{self.STATES[new_state]}**",
                timestamp=None,
            )
        )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @welcomer_group.command(
        name="channel", aliases=("ch",), brief="Changes the Welcomer channel"
    )
    async def welcomer_channel(self, ctx, channel: discord.TextChannel = None):
        """
        Changes the registered channel to where the Welcomer messages will be sent.
        This is how you turn the Welcomer the first time.
        """
        if channel is None:
            channel = ctx.channel

        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer is None:
            await self.bot.mongodb.guilds.update_one(
                {"_id": ctx.guild.id},
                {
                    "$set": {
                        "welcomer": {
                            "channel": channel.id,
                            "state": False,
                            "greetings": "",
                            "farewell": "",
                            "style": self.DEFAULT_STYLE,
                        }
                    }
                },
            )
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully set the welcomer channel to {channel.mention}"
                )
            )

        current_ch = discord.utils.get(ctx.guild.channels, id=welcomer["channel"])

        if current_ch == channel:
            raise UserWarning(
                f"{self.bot.crossmark} The channel was already set to {channel.mention}!"
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"welcomer.channel": channel.id}}
        )
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully changed the welcomer channel from {current_ch.mention} to {channel.mention}"
            )
        )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @welcomer_group.command(
        name="style", aliases=("s",), brief="Change the Welcomer style"
    )
    async def change_style(self, ctx, *, style: to_lower):
        """
        Changes the Welcomer message style. Currently there are two styles:
            • Detailed → Shows detailed info about the user that joined/left, such as the ID, 
            account creation date, time spent on the server, etc, as well as the member count.
            • Simple → Just shows the registered greeting or farewell.

        To choose the Detailed style either type `detailed` or just `d` as the style argument.
        To choose the Simple style you can either type `simple` or just `s`.
        """
        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer is None:
            raise UserWarning(
                f"{self.bot.crossmark} Cannot toggle the Welcomer as it hasn't been setup yet!\n"
                "Check out the `channel` command to start!"
            )

        if style not in self.VALID_STYLES:
            raise UserWarning(
                f"{self.bot.crossmark} `{style}` isn't a valid style!\nCheck this command's help page for more info."
            )

        if style == welcomer["style"]:
            raise UserWarning(
                f"{self.bot.crossmark} The welcomer was already set with the `{style}` style!"
            )

        if len(style) == 1:  # Extend to full name
            style = self.STYLE_ALIASES[style]

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"welcomer.style": style}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully changed the welcomer style to **{style.title()}**"
            )
        )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @welcomer_group.command(
        name="greeting", aliases=("g",), brief="Change this server's greeting"
    )
    async def change_greeting(self, ctx, *, greeting: str):
        """
        Change the greeting message that will be shown when a new user joins the guild.
        This is where you can give a little brief about your guild or do something else 
        creative. You have a maximum of 100 characters, though.
        To spice things more, there are three values you can insert into your message. These are:
            • Guild name → ``%%`` 
            • Member display name → ``$$``
            • Member name and tag → ``&&``
            • Member mention → ``@@``
        How do they work? It's really easy: you just write the underlined characters that correspond
        to whatever value you want to insert and the bot will process them!
        Here's an example:
            `Howdy &&! Welcome to %%, I hope you have a wonderful time! @@ Ping-pong :P`
        *in my server would make*
            `Howdy Toast#1234! Welcome to CyborgToast's Playground, I hope you have a wonderful time! @User Ping-pong :P`
        """
        if len(greeting) > self.MAX_MSG_LEN:
            raise UserWarning(
                f"{self.bot.crossmark} That message is too long! It must be 100 characters long at max."
            )

        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer is None:
            raise UserWarning(
                f"{self.bot.crossmark} Cannot toggle the Welcomer as it hasn't been setup yet!\n"
                "Check out the `channel` command to start!"
            )

        if greeting == welcomer["greetings"]:
            raise UserWarning(
                f"{self.bot.crossmark} That was the set greeting message already!"
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"welcomer.greeting": greeting}}
        )
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully changed the welcomer greeting!"
            )
        )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @welcomer_group.command(
        name="farewell", aliases=("f",), brief="Change this server's farewell"
    )
    async def change_farewell(self, ctx, *, farewell: str):
        """
        Change the greeting message that will be shown when a member leaves the guild.
        This is where you can say a proper good-bye or do something else creative. You have 
        a maximum of 100 characters, though.
        To spice things more, there are three values you can insert into your message. These are:
            • Guild name → ``%%`` 
            • Member display name → ``$$``
            • Member name and tag → ``&&``
            • Member mention → ``@@``
        How do they work? It's really easy: you just write the underlined characters that correspond
        to whatever value you want to insert and the bot will process them!
        Here's an example:
            `;( It's sad to see && go... We hope you might return to %% one day!`
        *in my server would make*
            `;( It's sad to see Toast#1234 go... We hope you might return to CyborgToast's Playground one day!`
        """
        if len(farewell) > self.MAX_MSG_LEN:
            raise UserWarning(
                f"{self.bot.crossmark} That message is too long! It must be 100 characters long at max."
            )

        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer is None:
            raise UserWarning(
                f"{self.bot.crossmark} Cannot toggle the Welcomer as it hasn't been setup yet!\n"
                "Check out the `channel` command to start!"
            )

        if farewell == welcomer["farewell"]:
            raise UserWarning(
                f"{self.bot.crossmark} That was the set farewell message already!"
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"welcomer.farewell": farewell}}
        )
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully changed the welcomer farewell!"
            )
        )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @welcomer_group.command(
        name="preview", aliases=("pv",), brief="Get a preview of your welcomer"
    )
    async def welcomer_preview(self, ctx, kind: to_lower):
        """
        This command will show you a preview of the Welcomer message type you specify.
        They must be `greeting` *(aliases: "greetings", "g")* or `farewell` *(alias: "f")*
        This will simulate you joining or leaving the guild with a random cause and reason (if it applies).
        This means that you can see the formatted version of your greeting/farewell messages, that follow this cheat sheet:
        `
        +===================+======+
        ‖ Value             ‖ Char ‖
        +===================+======+
        +==========================+
        ‖ Guild name        ‖  %%  ‖
        +===================+======+
        ‖ Member name       ‖  $$  ‖
        +===================+======+
        ‖ Member name + tag ‖  &&  ‖
        +===================+======+
        ‖ Member mention    ‖  @@  ‖
        +===================+======+
        `
        """
        types = ("greeting", "greetings", "g", "farewell", "f")
        if kind not in types:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} You must provide a valid welcome message type!"
                )
            )
        welcomer = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"welcomer": 1}
            )
        ).get("welcomer")
        if welcomer is None:
            raise UserWarning(
                f"{self.bot.crossmark} Cannot simulate the Welcomer as it hasn't been setup yet!\n"
                "Check out the `channel` command to start!"
            )
        if kind in types[:-2]:  # only greetings
            kind = "greetings"
            ch, msg, style = await self.get_welcomer_bits(kind, ctx.author)

            if style == "simple":
                emb = embeds.Embed(description=msg, colour=self.GREEN_HUE)

            else:
                description = dedent(
                    f"""
                    {msg}

                    **ID**: {ctx.author.id}
                    **Mention**: {ctx.author.mention}
                    **Account created on**: {ctx.author.created_at.strftime(r"%x at %X")}
                    """
                )
                emb = discord.Embed(description=description, colour=self.GREEN_HUE)
                emb.set_author(
                    name=f"{ctx.author} joined!", icon_url=ctx.guild.icon_url
                )
                emb.set_footer(
                    text=f"Member #{len(ctx.guild.members)} (of which {sum(m.bot for m in ctx.guild.members)} are bots)",
                    icon_url=ctx.guild.icon_url,
                )
                emb.set_thumbnail(url=ctx.author.avatar_url)

        if kind in types[4:]:  # only farewells
            kind = "farewell"
            ch, msg, style = await self.get_welcomer_bits(kind, ctx.author)

            if style == "simple":
                emb = embeds.Embed(description=msg, colour=self.RED_HUE)

            else:
                opts = ("just left!", "was kicked!", "was banned!")
                w_happened = choice(opts)
                emb = embeds.Embed(
                    title=f"{ctx.author} {w_happened}",
                    description=dedent(
                        f"""
                        {msg}
                        %REASON%
                        **Top role**: {ctx.author.top_role.mention if str(ctx.author.top_role) != '@everyone' else ctx.author.top_role}
                        **Mention**: {ctx.author.mention}
                        **ID**: {ctx.author.id}
                        **Account created at**: {ctx.author.created_at.strftime(r"%x at %X")}
                        **Joined here at**: {ctx.author.joined_at.strftime(r"%x at %X")}
                        **Was here for**: {self.pretty_timespan(datetime.utcnow() - ctx.author.joined_at)}
                        """
                    ),
                    colour=self.RED_HUE,
                )

                if w_happened != "just left!":
                    reasons = (
                        "S:b:AM",
                        "Didn't like cats",
                        "Wasn't eating toast",
                        "Too rude, m8",
                    )
                    reason = choice(reasons)
                    emb.description = emb.description.replace(
                        "%REASON%", f"\n**Reason:** {reason}\n"
                    )
                else:
                    emb.description = emb.description.replace("%REASON%", "")

                emb.set_author(name=f"{ctx.author} left!", icon_url=ctx.guild.icon_url)
                emb.set_footer(
                    text=f"{len(ctx.guild.members)} members remain (of which {sum(m.bot for m in ctx.guild.members)} are bots)",
                    icon_url=ctx.guild.icon_url,
                )
                emb.set_thumbnail(url=ctx.author.avatar_url)

        await ctx.send(
            content=f"Here's a preview of the current welcomer {kind}!", embed=emb
        )


def setup(bot):
    bot.add_cog(Welcomer(bot))
