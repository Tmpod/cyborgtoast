#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""General utility stuff"""

import discord
from discord.ext.commands import Cog
from libneko import commands
from urllib.parse import quote
import aiohttp
from libneko import embeds
from libneko.pag import navigator
from typing import Optional
from time import monotonic
from asyncio import TimeoutError

from toaster.utils import strings, misc, MessageConverter #, to_hex


@misc.with_category()
class Utility:
    """
    Some general utility commands
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=("sinfo", "si"), brief="General info about this server.")
    async def serverinfo(self, ctx):
        """Sends some general useful information about the current server."""
        guild = ctx.guild
        em = discord.Embed(
            description="Here's some general info about this guild!",
            color=misc.rand_hex(),
        )
        em.set_author(name=guild.name, icon_url=guild.splash_url)
        em.set_footer(text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004")
        em.set_thumbnail(url=guild.icon_url)
        em.add_field(name="Owner", value=guild.owner)
        em.add_field(name="ID", value=guild.id)
        em.add_field(name="Region", value=strings.titlefy(guild.region))
        em.add_field(name="Created on", value=guild.created_at.strftime("%x at %X"))
        em.add_field(
            name="Verification Level", value=strings.titlefy(guild.verification_level)
        )
        em.add_field(
            name="Content Filter", value=strings.titlefy(guild.explicit_content_filter)
        )
        em.add_field(name="Member Count", value=guild.member_count)
        em.add_field(
            name="Channels",
            value=f"Total:`{len(guild.channels)}`\nText:`{len(guild.text_channels)}`\nVoice:`{len(guild.voice_channels)}`",
        )
        em.add_field(name="Emojis", value=len(guild.emojis))
        em.add_field(
            name="Roles", value=f"Total:`{len(guild.roles)}`\nTop:`{guild.roles[-1]}`"
        )
        em.add_field(name="Default Channel", value=guild.system_channel)
        em.add_field(
            name="Features",
            value=f"`{strings.pretty_list(guild.features, sep='`, `')}`",
        )
        await ctx.send(embed=em)

    @commands.command(
        aliases=["a", "icon", "logo"], brief="Contemplate someone's avatar."
    )
    async def avatar(self, ctx, user: discord.Member = None):
        """Sends an enlarge version of the specified user. If none is specified, then the your avatar is displayed."""
        if user is None:
            em = discord.Embed(color=discord.Colour.dark_teal())
            em.set_image(url=ctx.author.avatar_url_as(size=1024))
            await ctx.send(embed=em)
        elif len(ctx.message.mentions) > 1:
            await ctx.send(
                embed=discord.Embed(
                    description=f"{self.bot.crossmark} You cannot enter more than one mention at a time!",
                    color=discord.Colour.red(),
                )
            )
        else:
            em = discord.Embed(color=discord.Colour.dark_teal())
            em.set_image(url=user.avatar_url_as(size=1024))
            await ctx.send(embed=em)

    @commands.command(aliases=["ex", "exam"], brief="Examine a user or a role.")
    async def examine(self, ctx, user: discord.Member = None):
        """Sends some information about the mention (either a user or a role)."""
        if user is None:
            await ctx.send(
                embed=discord.Embed(
                    description=f"{self.bot.crossmark} You haven't entered a mention!",
                    color=discord.Colour.red(),
                )
            )
        elif len(ctx.message.mentions) == 0:  # or len(user.split(' ')) > 1
            await ctx.send(
                embed=discord.Embed(
                    description=f"{self.bot.crossmark} You've entered an invalid mention!",
                    color=discord.Colour.red(),
                )
            )
        elif len(ctx.message.mentions) > 1:
            await ctx.send(
                embed=discord.Embed(
                    description=f"{self.bot.crossmark} You cannot enter more than one mention at a time!",
                    color=discord.Colour.red(),
                )
            )
        else:
            em = discord.Embed(
                title=f":mag_right: {user}",
                description="Here's some general info about this user!",
                color=discord.Colour.from_rgb(33, 99, 167),
            )
            em.set_footer(
                icon_url=self.bot.user.avatar_url,
                text="Thanks for using CyborgToast! | Made with ❤ by Tmpod#1004",
            )
            em.set_thumbnail(url=user.avatar_url)
            em.add_field(name="*ID*", value=user.id)
            em.add_field(name="*Created on*", value=user.created_at.strftime("%c"))
            em.add_field(
                name="*Joined this guild on*", value=user.joined_at.strftime("%c")
            )
            em.add_field(name="*Type*", value=user.bot and "Bot" or "Hooman")
            em.add_field(name="*Top role*", value=user.roles[1])
            em.add_field(name="*Status*", value=user.status)
            if user.activity.type == 0:
                em.add_field(name="*Activity*", value=f"Playing {user.activity.name}")
            elif user.activity.type == 1:
                em.add_field(name="*Activity*", value=f"Streaming {user.activity.name}")
            elif user.activity.type == 2:
                em.add_field(name="*Activity*", value=f"Listening {user.activity.name}")
            elif user.activity.type == 3:
                em.add_field(name="*Activity*", value=f"Watching {user.activity.name}")
            em.add_field(name="*Display Name*", value=user.nick)
            em.add_field(
                name="*Roles*", value=", ".join(map(str, reversed(user.roles)))[:1024]
            )

            await ctx.send(embed=em)

        # emDM = discord.Embed(description="Thanks for suggesting something to my creator! Now he'll take a look at your request and then give you an answer ASAP.", color=discord.Colour.dark_green())
        # emDM.set_author(name="Your suggestion for CyborgToast", icon_url=self.bot.user.avatar_url)
        # emDM.set_footer(text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004")
        # emDM.add_field(name="Suggestion Content", value=f"```{smth}```")
        # await ctx.author.send(embed=emDM)

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.command(
        name="qrcode",
        aliases=("qr",),
        brief="Turn your thoughts into white and black squares.",
    )
    async def qr_cmd(self, ctx, *, text: str):
        """Turns your message into a QR Code, using goqr API. Check it out ar goqr.me"""
        async with ctx.typing():
            em = discord.Embed(
                description=f"```{text}```", color=discord.Colour.darker_grey()
            )
            em.set_author(name="This QR code contains the folowing message:")
            em.set_footer(text="Generated using goqr API. Check it out at goqr.me")
            em.set_image(
                url=f"http://api.qrserver.com/v1/create-qr-code/?data={quote(text)}&size=1024x1024"
            )
            await ctx.message.delete()
            await ctx.send(embed=em)

    @commands.command(name="rgb2hex", brief="Converts rbg values to an hex value")
    async def rgb2hex_cmd(self, ctx, r: int, g: int, b: int):
        """Takes three values and turns them into a hex value."""
        hex_value = hex((r & 0xFF) << 16 | (g & 0xFF) << 8 | b & 0xFF)[2:][::-1].zfill(
            6
        )[::-1]

        await ctx.send(
            embed=discord.Embed(
                description=f"The RGB value **`{r} {g} {b}`** is the equivalent to **`{hex_value}`** in Hexadecimal notation.",
                color=discord.Colour.from_rgb(r, g, b),
            )
        )

    @commands.command(name="hex2rgb", brief="Converts a hex value to rgb values")
    async def hex2rgb_cmd(self, ctx, value: str):
        """Takes a hex value and turns it into an RGB tuple."""
        try:
            value = hex(int(value.replace("#", ""), 16))
        except (ValueError, TypeError):
            raise UserWarning(f"{self.bot.crossmark} That isn't a valid hex!")

        r, g, b = (value >> 16) & 0xFF, (value >> 8) & 0xFF, value & 0xFF

        await ctx.send(
            embed=discord.Embed(
                description=f"The Hexadecimal value **`{value}`** is equivalent to **`{r} {g} {b}`** in RGB.",
                color=discord.Colour.from_rgb(r, g, b),
            )
        )

    @commands.command(name="quote", brief="Quote a message ;)")
    async def quote_cmd(self, ctx, message: MessageConverter):
        """
        This will send a little embed with the quoted message contents (even embeds)
        You can specify what message to quote by either providing the message ID (only works
        for the channel the command is executed on) or by providing the message link (click the
        "Share link" button) which works everywhere.
        """
        await ctx.message.delete()

        if message.embeds:
            desc = strings.embed_to_str(message.embeds[0])
        else:
            desc = message.content

        top_role = message.author.roles[-1]
        colour = (
            top_role.colour if not top_role.is_default() else discord.Colour.default()
        )

        emb = embeds.Embed(
            description=f"\N{ZERO WIDTH SPACE}\n{desc}\n\n[*Jump to message*]({message.jump_url})",
            colour=colour,
            timestamp=message.created_at,
        )
        emb.set_author(
            name=message.author.display_name, icon_url=message.author.avatar_url
        )

        await ctx.send(embed=emb)

    @commands.command(name="spoiler", brief="Makes a spoiler message")
    async def spoiler_cmd(
        self, ctx, timeout: Optional[int] = 15, hint: str = None, *, text: str
    ):
        """This makes a little embed to which you have to react to see the whole message"""
        await ctx.message.delete()

        if len(hint) > 100:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} That hint is too long! It must be under 100 characters."
                )
            )

        mailbox = "\N{CLOSED MAILBOX WITH RAISED FLAG}"

        top_role = ctx.author.roles[-1]
        colour = (
            top_role.colour if not top_role.is_default() else discord.Colour.default()
        )

        emb = embeds.Embed(
            title=hint or "",
            description="Click the :mailbox: to see the spoiler!",
            colour=colour,
        )
        emb.set_author(name=ctx.author.display_name, icon_url=ctx.author.avatar_url)
        seal = await ctx.send(embed=emb)
        await seal.add_reaction(mailbox)

        start = monotonic()
        while (start - monotonic()) < timeout * 60:
            try:
                _, usr = await self.bot.wait_for(
                    "reaction_add",
                    check=lambda r, u: all(
                        (r.emoji == mailbox, r.message.id == seal.id, not u.bot)
                    ),
                    timeout=60,
                )
            except TimeoutError:
                continue
            else:
                pm_emb = emb
                pm_emb.description = text
                await usr.send(embed=emb)

        emb.set_footer(text="Expired!")
        await seal.clear_reactions()
        await seal.edit(embed=emb)


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Utility(bot))
