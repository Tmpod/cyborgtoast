#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import discord
from discord.ext.commands import Cog
from libneko import commands

from toaster.utils import with_category

@with_category()
class GoogleSearches:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(name="google", aliases=("search",), brief="Google searches", case_insensitive=True, invoke_without_command=True)
    async def google_searches(self, ctx, query: str = None):
    	...

    @google_searches.command(name="string", aliases=("txt",), brief="Googles for an text query")
    async def google_string(self, ctx, query: str):
    	...

    @google_searches.command(name="image", aliases=("img",), brief="Googles for an image")
    async def google_image(self, ctx, query: str):
    	...

def setup(bot):
    bot.add_cog(GoogleSearches(bot))