#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands, embeds
from typing import Union, Set

from toaster.utils import pretty_list, with_category


@with_category()
class AutoRole:
    VALID_SCOPES = ("global", "g", "humans", "h", "bots", "b")
    SCOPES_ALIASES = {"g": "global", "h": "humans", "b": "bots", "a": "all"}

    def __init__(self, bot):
        self.bot = bot

    def _get_roles(self, member: discord.Member, autorole: dict) -> Set[str]:
        """Gets the autorole scope(s)"""
        scopes_tests = {
            "humans": lambda m: not m.bot,
            "bots": lambda m: m.bot,
            "global": lambda _: True,
        }

        enabled_scopes = tuple(s for s, r in autorole.items() if r is not None)

        roles = set()
        for s, t in scopes_tests.items():
            if s in enabled_scopes and t(member):
                role = member.guild.get_role(autorole[s])
                if role is None:
                    continue
                roles.add(role)

        return roles

    async def on_member_join(self, member: discord.Member):
        """This handles the autorole distribuition"""
        autorole = (
            await self.bot.mongodb.guilds.find_one({"_id": member.guild.id})
        ).get("autorole")

        if not autorole:
            return

        roles = self._get_roles(member, autorole)
        await member.add_roles(*roles)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @commands.group(
        name="autorole",
        aliases=("atr",),
        brief="Give a role to newcomers!",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def autorole_group(self, ctx):
        """
        An autorole feature! What is it? An autorole is a registered role that gets added to new people
        joining the server. It's really useful and this implementation allows for three different scopes:
        __humans__, __bots__ and __global__. As you may guess from the naming, the __humans__ scope adds
        whatever role it has registered to human users, the __bots__ scope adds the regisred role to bots
        and the __global__ one adds it to every new memeber.
        """
        await ctx.invoke(self.autorole_overview)

    @autorole_group.command(
        name="overview", aliases=("ov",), brief="Overview of the autorole settings"
    )
    async def autorole_overview(self, ctx):
        """Lists the the roles registered for each scope"""
        autorole = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"autorole": 1}
            )
        ).get("autorole")

        if not autorole:
            desc = "`No autoroles set yet`"
        else:
            desc = ""
            for s, r in autorole.items():
                role = ctx.guild.get_role(r)
                if role:
                    desc += f"**{s.title()}:** {role.mention}\n"
                else:
                    desc += f"**{s.title()}:** `None set`\n"

        await ctx.send(
            embed=embeds.Embed(
                title="Here's the current config for the autorole", description=desc
            )
        )

    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @autorole_group.command(name="set", aliases=("s",), brief="Sets an autorole")
    async def set_autorole(self, ctx, role: discord.Role, scope: str = "global"):
        """
        Registers the given role as a role that will be given when a new user joins.
        There are three types that you can choose: __humans__, __bots__ or __global__.
        You can just pass that type after the role or ommit it to set it as global.
        If the name of the role you want to set contains spaces, put it inside quotation marks.
        """
        if scope not in self.VALID_SCOPES:
            raise UserWarning(
                f"{self.bot.crossmark} Invalid scope! Check out the command help page for more info!"
            )

        autorole = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"autorole": 1}
            )
        ).get("autorole")

        if len(scope) == 1:
            scope = self.SCOPES_ALIASES[scope]

        if autorole and autorole.get(scope) == role.id:
            raise UserWarning(
                f"{self.bot.crossmark} {role.mention} was already registerd for the `{scope}` scope!"
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {f"autorole.{scope}": role.id}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully set {role.mention} as the autorole for the `{scope}` scope!"
            )
        )

    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @autorole_group.command(
        name="revoke", aliases=("rv", "rm"), brief="Revokes an autorole"
    )
    async def revoke_autorole(self, ctx, scope: str = "global"):
        """
        Revokes the automation for the given role.
        There are four types you can from: __global__, __humans__, __bots__ or __all__.
        """
        if scope not in self.VALID_SCOPES and scope not in ("all", "a"):
            raise UserWarning(
                f"{self.bot.crossmark} Invalid scope! Check out the command help page for more info!"
            )

        autorole = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"autorole": 1}
            )
        ).get("autorole")

        if len(scope) == 1:
            scope = self.SCOPES_ALIASES[scope]

        if scope == "all":
            await self.bot.mongodb.guilds.update_one(
                {"_id": ctx.guild.id}, {"$set": {"autorole": {}}}
            )
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully revoked the all autoroles!"
                )
            )

        if autorole is None or autorole.get(scope) is None:
            raise UserWarning(
                f"{self.bot.crossmark} The autorole was already deactivated!"
            )

        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {f"autorole.{scope}": None}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully revoked the autorole for the `{scope}` scope!"
            )
        )


def setup(bot):
    bot.add_cog(AutoRole(bot))
