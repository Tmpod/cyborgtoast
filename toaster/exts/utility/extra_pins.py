#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Extra pins
"""

from discord.ext.commands import Cog
from toaster.utils import with_category

@with_category()
class ExtraPins:
	def __init__(self, bot):
		self.bot = bot


def setup(bot):
	bot.add_cog(ExtraPins(bot))
