#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
from discord.ext.commands import Cog
from libneko import commands, embeds
from libneko.pag import navigator
from typing import Union

from toaster.utils import with_category, pretty_list


@with_category()
class Tasks:
    def __init__(self, bot):
        self.bot = bot
        self.MAX_OPEN_TASKS = 20
        self.MAX_TITLE = 80
        self.MAX_DESC = 200

    @commands.group(
        name="tasks",
        aliases=("tsk",),
        brief="Organize yourself :D",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def tasks_group(self, ctx):
        await ctx.invoke(self.tasks_group)

    @tasks_group.command(name="list", aliases=("ls",), brief="Lists all your tasks")
    async def list_tasks(self, ctx):
        tasks = await self.bot.mongodb.tasks.find_one({"_id": ctx.author.id})

        emb = embeds.Embed()
        emb.set_author(name="Your Tasks", icon_url=ctx.author.avatar_url)

        if tasks is None:
            emb.description = "`You don't have any tasks yet!`"
            await ctx.send(embed=emb)

        page2 = emb

        open_tasks = (
            f"**{d['title']}**\n{d['desc']}" for d in tasks["open"]
        )
        emb.title = "Open Tasks"
        emb.description = pretty_list(open_tasks, sep="\n`--------------------`\n") or "`No open tasks`"

        done_tasks = (
            f"**{d['title']}**\n{d['desc']}" for d in tasks["done"]
        )
        page2.title = "Completed Tasks"
        page2.description = (
            pretty_list(done_tasks, sep="\n\n") or "`No completed tasks`"
        )

        await navigator.EmbedNavigator(ctx=ctx, pages=(emb, page2)).start()

    @tasks_group.command(
        name="add", aliases=("a", "create"), brief="Creates a new task"
    )
    async def add_task(self, ctx, title: str, *, description: str = None):
        if len(title) > self.MAX_TITLE or len(description) > self.MAX_DESC:
            raise UserWarning(
                f"{self.bot.crossmar} Title/description too long!\n"
                f"The title must be under **{self.MAX_TITLE}** characters long"
                f"and the description must be under **{self.MAX_DESC}**."
            )

        tasks = await self.bot.mongodb.tasks.find_one({"_id": ctx.author.id})

        if tasks is not None and len(tasks["open"]) >= self.MAX_OPEN_TASKS:
            raise UserWarning(
                f"{self.bot.crossmar} You cannot have more than **20** open tasks at the same time!"
            )

        if tasks is None:
            await self.bot.mongodb.tasks.insert_one(
                {
                    "_id": ctx.author.id,
                    "open": [{"title": title, "desc": description}],
                    "done": [],
                }
            )

        else:
            await self.bot.mongodb.tasks.update_one(
                {"_id": ctx.author.id},
                {
                    "$set": {
                        "open": tasks["open"] + [{"title": title, "desc": description}]
                    }
                },
            )

        await ctx.send(
            embed=embeds.Embed(
                description=(
                    f"{self.bot.checkmark} Successfully added a new task with the following fields:\n"
                    f"Title: `{title}`\nDescrption: `{description}`"
                ),
                timestamp=None
            )
        )

    @tasks_group.command(
        name="complete", aliases=("end",), brief="Completes the given task"
    )
    async def complete_task(self, ctx, task: Union[int, str]):
        tasks = await self.bot.mongodb.tasks.find_one({"_id": ctx.author.id})
        
        if tasks is None or not tasks["open"]:
            raise UserWarning(f"{self.bot.crossmark} You don't have any open tasks!")

        # if 

    @tasks_group.command(
        name="edit", aliases=("e", "modify", "mod"), brief="Edits the given task"
    )
    async def edit_task(self, ctx, task: str, title: str, description: str = None):
        ...

    @tasks_group.command(
        name="info", aliases=("i",), brief="Checks more info about the given task"
    )
    async def task_info(self, ctx, task: str):
        ...


def setup(bot):
    bot.add_cog(Tasks(bot))
