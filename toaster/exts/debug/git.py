#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This is an extension that has some git related features
This was made by Dusty.P (https://gitlab.com/Dusty.P) for the Dev-Sebi project (https://gitlab.com/Dusty.P/Dev-Sebi) where I'm a developer
I made some adaptations for this to work with libneko's embed factory and I also did some other bits and bobs to make this fit more with CyborgToast
Still, all credit for the `git pull` and `git status` commands goes to Dusty
"""

import discord
from discord.ext.commands import Cog
from libneko import commands

from toaster.utils import run_in_shell, with_category
import asyncio
from libneko.pag.factory import embedfactory
from libneko import embeds

from toaster import __url__


@with_category()
class Git:
    """
    This cog holds a git command group which can provide the link to my GitLab repository, 
    pull code from it and check the current status
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        name="git",
        aliases=("g", "repo"),
        brief="GitSauce is love. GitSauce is life",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def git_group(self, ctx):
        """
        This holds some git related features.
        If you don't run any subcommand (or run the `link` one), you'll get my GitLab repository link!
        The other features are exclusive to my master ;)
        """
        await ctx.invoke(self.repo_link)

    @git_group.command(
        name="link", aliases=("l", "repo"), brief="Sends my GitLab repository link!"
    )
    async def repo_link(self, ctx):
        """Sends the link for my GitLab repository. Feel free to open issues or pull requests."""
        await ctx.send(
            embed=embeds.Embed(title=f"{self.bot.custom_emojis.i_dot} Here's my GitLab repo!", description=__url__)
        )

    @commands.is_owner()
    @commands.cooldown(rate=1, per=10, type=commands.BucketType.default)
    @git_group.command(
        name="reset", aliases=("rs",), brief="Resets the repo to be like the remote"
    )
    async def git_reset(self, ctx):
        """This fetch the remote repo and reset the local one"""
        self.bot.logger.warning("Invoking git fetch and reset")
        await ctx.trigger_typing()
        # Pretty sure you can just do await run_in_shell() if that is async,
        # or run in a TPE otherwise.
        result = (
            await asyncio.wait_for(
                self.bot.loop.create_task(run_in_shell("git fetch --all")), 120
            )
            + "\n"
        )
        result += (
            await asyncio.wait_for(
                self.bot.loop.create_task(
                    run_in_shell(
                        "git reset --hard origin/$(git rev-parse "
                        "--symbolic-full-name --abbrev-ref HEAD)"
                    )
                ),
                120,
            )
            + "\n\n"
        )
        result += await asyncio.wait_for(
            self.bot.loop.create_task(
                run_in_shell('git show --stat | sed "s/.*@.*[.].*/ /g"')
            ),
            10,
        )

        await self.bot.count_loc()  # Recalculating the loc and updating the propery

        nav = embedfactory.EmbedNavigatorFactory(
            prefix=f"__**Git Fetch and Reset**__ ```", suffix="```"
        )
        nav.add_block(result)
        nav.build(ctx).start()

    @commands.is_owner()
    @commands.cooldown(rate=1, per=10, type=commands.BucketType.default)
    @git_group.command(
        name="pull", aliases=("p",), brief="Pulls the newst code from GitLab"
    )
    async def git_pull(self, ctx):
        """This will pull code from my repo and update the bot files"""
        self.bot.logger.warning("Invoking git-pull")
        await ctx.trigger_typing()
        # Using wait_for so that we can see if the something is wrong with the pull
        result = (
            await asyncio.wait_for(
                self.bot.loop.create_task(run_in_shell("git pull")), 120
            )
            + "\n"
        )
        result += await asyncio.wait_for(
            self.bot.loop.create_task(
                run_in_shell('git show --stat | sed "s/.*@.*[.].*/ /g"')
            ),
            10,
        )

        await self.bot.count_loc()  # Recalculating the loc and updating the propery

        nav = embedfactory.EmbedNavigatorFactory(
            prefix=f"__**Git Pull**__ ```", suffix="```"
        )
        nav.add_block(result)
        nav.build(ctx).start()

    @commands.is_owner()
    @commands.cooldown(rate=1, per=10, type=commands.BucketType.default)
    @git_group.command(name="status", aliases=("s",), brief="Check the commit status")
    async def git_status(self, ctx):
        """This will check the current git status"""
        await ctx.trigger_typing()
        # em.set_thumbnail(url=f"{ctx.guild.me.avatar_url}")
        result = await asyncio.wait_for(
            self.bot.loop.create_task(run_in_shell("git status")), 10
        )

        nav = embedfactory.EmbedNavigatorFactory(
            prefix=f"__**Git Status**__ ```diff", suffix="```"
        )
        nav.add_block(result)
        nav.build(ctx).start()


def setup(bot):
    bot.add_cog(Git(bot))
