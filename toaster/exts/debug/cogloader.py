#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands

from libneko import embeds
from traceback import print_exception
from toaster.utils import pretty_list, get_ext_path, with_category
from textwrap import dedent


@with_category()
class CogLoader:
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.command(
        name="reload",
        aliases=("rld",),
        brief="Reloads the given extension(s)",
        hidden=True,
    )
    async def reload_cog(self, ctx, *, cog: str):
        """
        Reloads the specified extension if it is in the `extensions.txt` file. 
        Specify `all` to reload all extensions
        """
        await ctx.trigger_typing()
        ext = await get_ext_path(self.bot, cog)(cog)
        if isinstance(ext, str):
            self.bot.unload_extension(ext)
            self.bot.load_extension(ext)
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully reloaded **{cog}** !",
                    color=discord.Colour.green(),
                )
            )
            self.bot.logger.info(f"Reloaded: {ext}")
        elif cog == "all":
            exts = await get_ext_path(self.bot, cog)(cog)
            for c in exts:
                try:
                    self.bot.unload_extension(c)
                    self.bot.load_extension(c)
                except BaseException as ex:
                    self.bot.failed_exts[c] = ex
            report_notice = ""
            if self.bot.failed_exts:
                report_notice = "A report has been sent to you!"
                emb = embeds.Embed(
                    title=":page_with_curl: Error report (loading/unloading cogs)"
                )
                for cog in self.bot.failed_exts:
                    exc = self.bot.failed_exts[cog]
                    emb.add_field(
                        name=cog,
                        value=(
                            "```py\n"
                            + "\n".join(
                                format_exception(type(exc), exc, exc.__traceback__)
                            )
                            + "\n```"
                        ),
                    )
                await self.bot.logs_channel.send(embed=emb)
                self.bot.failed_exts = {}  # resetting the cache
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Reloaded all cogs with **{len(self.bot.failed_exts) or 'no'}** errors! "
                    f"{report_notice}",
                    color=discord.Colour.green(),
                )
            )
            self.bot.logger.info(
                f"Reloaded: ALL with {len(self.bot.failed_exts)} errors"
            )
        else:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} That cog name is not valid!",
                    color=discord.Colour.red(),
                )
            )

    @commands.is_owner()
    @commands.command(
        name="unload",
        aliases=("uld",),
        brief="Unloads the given extension(s)",
        hidden=True,
    )
    async def unload_cog(self, ctx, *, cog: str):
        """
        Unloads the specified extension if it is in the `extensions.txt` file. 
        Specify `all` to unload all extensions *(except `cogloader`)*
        """
        await ctx.trigger_typing()
        ext = await get_ext_path(self.bot, cog)(cog)
        if isinstance(ext, str):
            self.bot.unload_extension(ext)
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully unloaded **{cog}** !",
                    color=discord.Colour.green(),
                )
            )
            self.bot.logger.info(f"Unloaded: {ext}")
        elif cog == "all":
            exts = await get_ext_path(self.bot, cog)(cog)
            for c in exts:
                if c.endswith("cogloader"):
                    continue
                try:
                    self.bot.unload_extension(c)
                except BaseException as ex:
                    self.bot.failed_exts[c] = ex
            report_notice = ""
            if self.bot.failed_exts:
                report_notice = "A report has been sent to you!"
                emb = embeds.Embed(
                    title=":page_with_curl: Error report (unloading cogs)"
                )
                for cog in self.bot.failed_exts:
                    exc = self.bot.failed_exts[cog]
                    emb.add_field(
                        name=cog,
                        value=dedent(
                            f"""
                        ```py
                        {print_exception(type(exc), exc, exc.__traceback__)}
                        ```
                        """
                        ),
                    )
                await self.bot.logs_channel.send(
                    embed=emb, content=f"{self.bot.creator.mention}"
                )
                self.bot.failed_exts = {}  # resetting the cache
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Unloaded all cogs with **{len(self.bot.failed_exts) or 'no'}** errors! "
                    f"{report_notice}",
                    color=discord.Colour.green(),
                )
            )
            self.bot.logger.info(
                f"Unloaded: ALL (except loader) with {len(self.bot.failed_exts)} errors"
            )
        else:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} That cog name is not valid!",
                    color=discord.Colour.red(),
                )
            )

    @commands.is_owner()
    @commands.command(
        name="load", aliases=("ld",), brief="Loads the given extension(s)", hidden=True
    )
    async def load_cog(self, ctx, *, cog: str):
        """
        Loads the specified extension if it is in the `extensions.txt` file. 
        Specify `all` to load all extensions
        """
        await ctx.trigger_typing()
        ext = await get_ext_path(self.bot, cog)(cog)
        if isinstance(ext, str):
            self.bot.load_extension(ext)
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully loaded **{cog}** !",
                    color=discord.Colour.green(),
                )
            )
            self.bot.logger.info(f"Loaded: {ext}")
        elif cog == "all":
            exts = await get_ext_path(self.bot, cog)(cog)
            for c in exts:
                try:
                    self.bot.load_extension(c)
                except BaseException as ex:
                    self.bot.failed_exts[c] = ex
            report_notice = ""
            if self.bot.failed_exts:
                report_notice = "A report has been sent to you!"
                emb = embeds.Embed(title=":page_with_curl: Error report (loading cogs)")
                for cog in self.bot.failed_exts:
                    exc = self.bot.failed_exts[cog]
                    emb.add_field(
                        name=cog,
                        value=dedent(
                            f"""
                        ```py
                        {print_exception(type(exc), exc, exc.__traceback__)}
                        ```
                        """
                        ),
                    )
                await self.bot.logs_channel.send(
                    embed=emb, content=f"{self.bot.creator.mention}"
                )
                self.bot.failed_exts = {}  # resetting the cache
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Loaded all cogs with **{len(self.bot.failed_exts) or 'no'}** errors! "
                    f"{report_notice}",
                    color=discord.Colour.green(),
                )
            )
            self.bot.logger.info(f"Loaded: ALL with {len(self.bot.failed_exts)} errors")
        else:
            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} That cog name is not valid!",
                    color=discord.Colour.red(),
                )
            )

    @commands.command(
        name="extls",
        aliases=("cogls",),
        brief="Shows a list of all the bot's extensions",
    )
    async def exts_list(self, ctx, options: str = None):
        """
        Displays a little list of all the currently loaded extensions.
        Pass `--full` or `all` to see all the extensions that are loaded by default.
        """
        if options:
            if options.lower() in ("--full", "-f", "all"):
                which_cogs = "default"
                cog_list = pretty_list(
                    {e.split(".")[-1] for e in self.bot.default_exts}, sep="\n"
                )
            else:
                return await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.crossmark} Invalid option! It must be either `--full` or `all`",
                        colour=discord.Colour.red(),
                    )
                )
        else:
            which_cogs = "currently loaded"
            cog_list = pretty_list(
                {e.split(".")[-1] for e in self.bot.extensions}, sep="\n"
            )

        await ctx.send(
            embed=embeds.Embed(
                title=f"Here's the full list of all my {which_cogs} extensions!",
                description=f"```{cog_list}```",
            )
        )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(CogLoader(bot))
