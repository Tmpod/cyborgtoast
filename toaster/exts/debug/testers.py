#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands, embeds

from toaster import __url__
from toaster.utils import with_category


def is_tester():
    def decorator(ctx):
        if ctx.author.id in ctx.bot.config.toast.testers:
            return True
        return False

    return commands.check(decorator)

@with_category()
class Testers:
    def __init__(self, bot):
        self.bot = bot

    @is_tester()
    @commands.group(name="testers", aliases=("tst",), brief="Commands for certified testers", case_insensitive=True)
    async def testers_group(self, ctx):
        """A bunch of tools granted to certified testers"""
        pass
    
    
    @testers_group.command(name="bug", brief="Report bugs")
    async def report_bugs(self, ctx, *, text: str):
        """Sends a bug report to my creator."""
        emb = embeds.Embed(
            description=f"```md\n{text}```"
        )
        emb.set_author(name=f"{ctx.author} bug report", icon_url=ctx.author.avatar_url)
        msg = await self.bot.suggestions_log.send(embed=emb)

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully sent bug report to my creator!",
                colour=discord.Colour.green(),
            )
        )

def setup(bot):
    bot.add_cog(Testers(bot))