#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Bot Managing features for the CyborgToast's Playground server
"""

import discord
from discord.ext.commands import Cog
from libneko import commands
from libneko import embeds
import re
import secrets  # :p
from textwrap import dedent
from typing import Union

from toaster.utils import with_category, confirm_opts

MAX_PREFIX_LENGTH = 5

PREFIX_PATTERN = re.compile(r"^\[([^\]]+)\]\s+(.+)$")

MAX_BOT_COUNT = 10


@with_category()
class BotManager:
    def __init__(self, bot):
        self.bot = bot
        bot.loop.create_task(self._finish_setup())

    async def _finish_setup(self):
        await self.bot.wait_until_ready()
        self.CTP_GUILD = self.bot.get_guild(self.bot.config.toast.ctp.guild_id)
        self.INVITE_LOG = self.CTP_GUILD.get_channel(self.bot.config.toast.ctp.invite_log)
        self.STAFF_ROLE = self.CTP_GUILD.get_role(self.bot.config.toast.ctp.staff)
        self.STAFF_BOTS_ROLE = self.CTP_GUILD.get_role(self.bot.config.toast.ctp.staff_bots)
        self.BOTS_PLUS_ROLE = self.CTP_GUILD.get_role(self.bot.config.toast.ctp["bots+"])
        self.BOTS_ROLE = self.CTP_GUILD.get_role(self.bot.config.toast.ctp.bots)
        self.BOT_DEV_ROLE = self.CTP_GUILD.get_role(self.bot.config.toast.ctp.bot_dev)

    async def __local_check(self, ctx):
        if ctx.guild.id == self.CTP_GUILD.id:
            return True
        raise commands.CheckFailure(message=(
            "This category can only be used in my support server!\n"
            f"You use the `{ctx.prefix}support` command to get an invite for it."
        ))

    async def scrape_log(
        self, bot_user: discord.Member
    ) -> Union[discord.Message, None]:
        """
        This will scrape the log channel and return a tuple containing a bool that represents the success of the scrape 
        and the message containing the invite for the passed bot account if it found one.
        """
        async for msg in self.INVITE_LOG.history(
            limit=15
        ):  # I don't think there's need to search more than that
            if msg.embeds:  # Ignoring all messages that are not embeds
                emb = msg.embeds[
                    0
                ]  # Since all embeds in this channel are from Dev-Sebi there'll will only be 1 embed per msg
                for f in emb.fields:
                    if f.name == "Bot ID" and f.value == f"`{bot_user.id}`":
                        return msg

    async def on_member_join(self, member: discord.Member):
        if not member.bot or member.guild.id != self.bot.config.toast.support_server:
            return            

        bot_data = await self.bot.mongodb.ctp_bots.find_one({"_id": member.id})

        if bot_data is None:
            return await member.kick(reason="The bot was not on the database")

        # Adding the confirmation reaction to the invite log
        inv_log = await self.scrape_log(member)
        if inv_log:
            await inv_log.add_reaction(self.bot.checkmark)

        bot_owner = member.guild.get_member(bot_data["owner"])
        roles = {self.BOTS_ROLE}
        if self.STAFF_ROLE in bot_owner.roles:
            roles.add(self.STAFF_BOTS_ROLE)
            roles.add(self.BOTS_PLUS_ROLE)

        await member.add_roles(*roles, reason="New bot joined")
        await member.edit(nick=f"[{bot_data['prefix']}] {member.display_name}")

        await bot_owner.add_roles(self.BOT_DEV_ROLE)

        async with await self.bot.baker.rsc.request("GET", "invite_notifs.txt") as fp:
            notifs = await fp.read()
            conf_notif = notifs.strip().split("===\n")[0]

            replacements = {
                "USER": bot_owner.name,
                "INVITE_TOKEN": bot_data["invite_token"],
            }
            for r, v in replacements.items():
                conf_notif = conf_notif.replace(f"{{{r}}}", v)

            emb = embeds.Embed(
                title="Bot Invitation Notification",
                description=conf_notif.split("---")[0],
            )
            emb.set_footer(
                text=conf_notif.split("---")[1], icon_url=member.guild.icon_url
            )

            await bot_owner.send(embed=emb)

    async def on_member_remove(self, member: discord.Member):
        if member.guild.id != self.bot.config.toast.support_server:
            return

        if member.bot:  # Remove it's record
            await self.bot.mongodb.ctp_bots.delete_one({"_id": member.id})
        else:  # Kick their potential bots. Their records will be handled above
            # Checking if the user had any bots
            bots = await (self.bot.mongodb.ctp_bots.find({"owner": member.id})).to_list(
                MAX_BOT_COUNT
            )  # ig they won't have more than those bots

            if not bots:  # End this here if they don't own any bots
                return

            for r in bots:  # Kick every bot registered
                await member.guild.get_member(r["_id"]).kick(
                    reason="Owner left or was kicked"
                )

    async def on_member_ban(self, guild: discord.Guild, member: discord.Member):
        """Pretty similar to the on member remove, but it will ban the bots instead"""
        if member.guild.id != self.bot.config.toast.support_server:
            return
            
        if member.bot:  # Remove it's record
            await self.bot.mongodb.ctp_bots.delete_one({"_id": member.id})
        else:  # Kick their potential bots. Their records will be handled above
            # Checking if the user had any bots
            bots = await (self.bot.mongodb.ctp_bots.find({"owner": member.id})).to_list(
                MAX_BOT_COUNT
            )

            if not bots:  # End this here if they don't own any bots
                return

            for r in bots:  # Kick every bot registered
                await member.guild.get_member(r["_id"]).ban(
                    reason="Owner left or was kicked"
                )

    @commands.group(
        name="botmanager",
        aliases=("bm",),
        brief="Bot manager features for my support server",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def bot_manager(self, ctx):
        h = ctx.bot.get_command("help")
        await ctx.invoke(h, query="botmanager")

    @bot_manager.command(
        name="claim", aliases=("gimme",), brief="Claims an unclaimed bot"
    )
    async def claim_bot(self, ctx, bot: discord.Member, owner: Union[discord.Member, str] = None):
        if not bot.bot:
            raise UserWarning(f"{self.bot.crossmark} You can only claim bot users...")

        if isinstance(owner, str) and owner.upper() != "STAFF":
            raise UserWarning(f"{self.bot.crossmark} Invalid owner!")

        if bot not in ctx.guild.members:
            raise UserWarning(f"{self.bot.maybemark} That bot isn't here!", "gold")

        if (
            await self.bot.mongodb.ctp_bots.count_documents({"owner": ctx.author.id})
            >= MAX_BOT_COUNT
            and self.STAFF_ROLE not in ctx.author.roles
        ):
            raise UserWarning(
                (
                    f"{self.bot.crossmark} You've already reached the max bot count!\n"
                    "The limit is `10` as that is the limit enforced by the Discord API"
                )
            )

        bot_data = await self.bot.mongodb.ctp_bots.find_one({"_id": bot.id})
        if not bot_data:
            matches = PREFIX_PATTERN.findall(bot.display_name)
            if not matches:
                raise UserWarning(
                    f"{self.bot.crossmark} Cannot detect prefix in bot's nick. Please report back to a staff member"
                )
            prefix, _ = matches[0]

            await self.bot.mongodb.ctp_bots.insert_one(
                {"_id": bot.id, "owner": ctx.author.id, "prefix": prefix}
            )

            return await ctx.send(
                embed=embeds.Embed(
                    title="Bot Claimed",
                    description=dedent(
                        f"""
                        You have claimed **{bot.display_name}** with a prefix of `{prefix}`.
                        If there is an error please run command again to correct the prefix
                        for `{ctx.prefix}botmanager unclaim {bot.mention}` to unclaim the bot.
                        """
                    ),
                )
            )

        if not bot_data["owner"]:
            matches = PREFIX_PATTERN.findall(bot.display_name)
            if not matches:
                raise UserWarning(
                    f"{self.bot.crossmark} Cannot detect prefix in bot's nick. Please report back to a staff member"
                )
            prefix, _ = matches[0]

            await self.bot.mongodb.ctp_bots.update_one(
                {"_id": bot.id}, {"$set": {"owner": ctx.author.id, "prefix": prefix}}
            )

            return await ctx.send(
                embed=embeds.Embed(
                    title="Bot Claimed",
                    description=dedent(
                        f"""
                        You have claimed **{bot.display_name}** with a prefix of `{prefix}`.
                        If there is an error run `{ctx.prefix}botmanager unclaim {bot.mention}`
                        to unclaim the bot.
                        """
                    ),
                )
            )

        if bot_data["owner"] != ctx.author.id:
            raise UserWarning(
                (
                    f"{self.bot.crossmark} That bot is already claimed!\n"
                    "If it's actually your bot please report this issue to the staff team!"
                )
            )

        if bot_data["owner"] == ctx.author.id:
            matches = PREFIX_PATTERN.findall(bot.display_name)
            if not matches:
                raise UserWarning(
                    f"{self.bot.crossmark} Cannot detect prefix in bot's nick. Please report back to a staff member"
                )
            prefix, _ = matches[0]

            old_prefix = bot_data["prefix"]
            maybe_not = prefix == old_prefix

            if not maybe_not:
                await self.bot.mongodb.ctp_bots.update_one(
                    {"_id": bot.id}, {"$set": {"prefix": prefix}}
                )

            return await ctx.send(
                embed=embeds.Embed(
                    title="Bot already claimed",
                    description=dedent(
                        f"""
                You are already this bot's owner!
                However, I've tried updating the registered prefix, to match the bot's nick.
                {f'*Changed it from `{old_prefix}` to `{prefix}`*' if maybe_not else ''}
                """
                    ),
                )
            )

    @bot_manager.command(
        name="unclaim", aliases=("ewwno",), brief="Unlaims a claimed bot of yours"
    )
    async def unclaim_bot(self, ctx, *, bot: discord.Member):
        if not bot.bot:
            raise UserWarning(f"{self.bot.crossmark} You can only claim bot users...")

        if bot not in ctx.guild.members:
            raise UserWarning(f"{self.bot.maybemark} That bot isn't here!", "gold")

        if not await self.bot.mongodb.ctp_bots.count_documents(
            {"owner": ctx.author.id}
        ):
            raise UserWarning(f"{self.bot.crossmark} You don't have any claimed bots!")

        bot_data = await self.bot.mongodb.ctp_bots.find_one({"_id": bot.id})
        if not bot_data or not bot_data["owner"]:
            raise UserWarning(f"{self.bot.crossmark} That bot is already unclaimed!")

        if (
            bot_data["owner"] != ctx.author.id
            and not ctx.author.guild_permissions.manage_server
        ):
            raise UserWarning(
                f"{self.bot.crossmark} That bot is claimed by someone else, and so you can't unclaim it!"
            )

        await self.bot.mongodb.ctp_bots.update_one(
            {"_id": bot.id}, {"$set": {"owner": None}}
        )

        await ctx.send(
            embed=embeds.Embed(
                title="Bot Unclaimed",
                description=dedent(
                    f"""
                    You have unclaimed **{bot.display_name}**.
                    If there is an error you can always run `{ctx.prefix}botmanager claim {bot.mention}` 
                    to claim the bot.
                    """
                ),
            )
        )

    @bot_manager.command(
        name="invite", aliases=("in",), brief="Places an invite for a bot"
    )
    async def invite_bot(self, ctx, bot: int, *, prefix: str):
        async with ctx.typing():
            try:
                bot = await self.bot.get_user_info(bot)
            except discord.errors.NotFound:
                raise UserWarning(f"{self.bot.crossmark} That isn't a valid user!")

            if not bot.bot:
                raise UserWarning(
                    f"{self.bot.crossmark} You can only invite bot users..."
                )

            if (
                bot in ctx.guild.members
                or await self.bot.mongodb.ctp_bots.count_documents({"_id": bot.id})
            ):
                raise UserWarning(
                    f"{self.bot.maybemark} That bot is already here (or it's still being evaluated)!",
                    "gold",
                )

            if len(prefix) > MAX_PREFIX_LENGTH:
                raise UserWarning(
                    f"{self.bot.crossmark} That prefix is too long. It must be {MAX_PREFIX_LENGTH} chars long at max!"
                )

            invite_token = secrets.token_hex(4).upper()
            await self.bot.mongodb.ctp_bots.insert_one(
                {
                    "_id": bot.id,
                    "owner": ctx.author.id,
                    "prefix": prefix,
                    "invite_token": invite_token,
                }
            )

            inv_url = discord.utils.oauth_url(bot.id, permissions=None, guild=ctx.guild)

            emb = embeds.Embed(
                title="Bot Invite Request",
                description=f"[Invite Link]({inv_url}) *(`{inv_url}`)*",
            )
            emb.set_thumbnail(url=bot.avatar_url)
            emb.set_footer(
                text=f"Invite code: {invite_token}", icon_url=ctx.author.avatar_url
            )

            emb.add_field(name="Bot Username", value=str(bot), inline=True)
            emb.add_field(name="Bot ID", value=f"`{bot.id}`", inline=True)
            emb.add_field(name="Bot Owner", value=ctx.author.mention, inline=True)
            emb.add_field(name="Bot Prefix", value=f"`{prefix}`", inline=True)

            await self.INVITE_LOG.send(embed=emb)

            await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully placed invite request! See your DMs for more info on it.",
                    timestamp=None,
                )
            )

        async with ctx.author.typing():
            async with await self.bot.baker.rsc.request(
                "GET", "invite_notifs.txt"
            ) as fp:
                notifs = await fp.read()
                inv_notif = notifs.strip().split("===\n")[0]

                replacements = {"USER": ctx.author.name, "INVITE_TOKEN": invite_token}
                for r, v in replacements.items():
                    inv_notif = inv_notif.replace(f"{{{r}}}", v)

                emb = embeds.Embed(
                    title="Bot Invitation Notification",
                    description=inv_notif.split("---")[0],
                )
                emb.set_footer(
                    text=inv_notif.split("---")[1], icon_url=ctx.guild.icon_url
                )

                await ctx.author.send(embed=emb)

    @bot_manager.command(
        name="kick", aliases=("k",), brief="Kicks a bot of yours from the server"
    )
    async def kick_bot(
        self, ctx, bot: discord.Member, *, reason: str = "Owner is bad owner :/"
    ):
        if not bot.bot:  # meh naming ik
            raise UserWarning(f"{self.bot.crossmark} You can only manage bots...")

        owner_id = (await self.bot.mongodb.ctp_bots.find_one({"_id": bot.id})).get("owner")
        if owner_id != ctx.author.id:
            raise UserWarning(f"{self.bot.crossmark} You are not this bot's owner!")

        confirm_m = await ctx.send(
            embed=embeds.Embed(
                title=f"Are you sure you want to kick {bot.name}?",
                description=f"Hit {self.bot.checkmark} if you are sure, and hit {self.bot.crossmark} "
                "or wait 10 seconds if you want to cancel this action.",
                colour=0xF5BD23,  # nice yellow hue
            )
        )
        decision = await confirm_opts(self.bot, confirm_m, ctx.author)
        await confirm_m.clear_reactions()

        if decision:
            await bot.kick(reason=reason)
            await confirm_m.edit(
                embed=embeds.Embed(
                    title=f"Successfully kicked {bot.name}!",
                    description="You can still invite it back with the `invite` command if you wish to do so.",
                    colour=discord.Colour.green(),  # I like this green
                )
            )

        else:
            await confirm_m.edit(
                embed=embeds.Embed(
                    title=f"Canceled the operation!",
                    description=f"You can still invite it back with the `{ctx.prefix}bm invite` command if you wish to do so.",
                    colour=discord.Colour.red(),  # I like this red too
                )
            )

    @bot_manager.command(
        name="prefix",
        aliases=("pf",),
        brief="Changes the registered prefix for the given bot",
    )
    async def change_bot_prefix(self, ctx, bot: discord.Member, *, prefix: str):
        if not bot.bot:
            raise UserWarning(f"{self.bot.crossmark} You can only manage bots...")

        owner_id = await self.bot.mongodb.ctp_bots.find_one({"_id": bot.id})
        if owner_id != ctx.author.id:
            raise UserWarning(f"{self.bot.crossmark} You are not this bot's owner!")

        if len(prefix) > MAX_PREFIX_LENGTH:
            raise UserWarning(
                f"{self.bot.crossmark} That prefix is too long. It must be {MAX_PREFIX_LENGTH} chars long at max!"
            )

        await bot.edit(nick=f"[{prefix}] {bot.display_name}")
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Changed {bot.mention} prefix from `{PREFIX_PATTERN.findall(bot.display_name)[0]}` to `{prefix}`"
            )
        )

    @bot_manager.command(
        name="claims",
        aliases=("cl",),
        brief="Lists the claimed bots for the given member",
    )
    async def claims_list(self, ctx, *, member: discord.Member = None):
        if member is None:
            member = ctx.author

        if member.bot:
            raise UserWarning(f"{self.bot.crossmark} Bots cannot own other bots...")

        if not member:
            member = ctx.author

        bots = await (self.bot.mongodb.ctp_bots.find({"owner": member.id})).to_list(
            MAX_BOT_COUNT
        )

        if not bots:
            raise UserWarning(
                f"{'You' if member == ctx.author else member.mention} have not claimed/invited any bots yet."
            )

        emb = embeds.Embed(
            title="Claims List",
            description=f"These are {'your' if member == ctx.author else member.mention} bots:",
        )

        for b in bots:
            m = ctx.guild.get_member(b["_id"])

            if m is None:
                continue

            emb.add_field(name=m.display_name, value=f"Stored Prefix: `{b['prefix']}`")

        if not emb.fields:
            emb.description = f"{emb.description}\n`None yet`"

        await ctx.send(embed=emb)

    @bot_manager.command(
        name="owner",
        aliases=("whoowns", "whowns"),
        brief="Checks who owns the given bot",
    )
    async def who_owns(self, ctx, *, bot: discord.Member):
        if not bot.bot:
            raise UserWarning(
                f"{self.bot.crossmark} You can only search for bot users..."
            )

        owner = (await self.bot.mongodb.ctp_bots.find_one({"_id": bot.id})).get("owner")

        if owner is None:
            await self.INVITE_LOG.send(
                embed=embeds.Embed(
                    description=f":warning: No one appears to own {bot.mention}"
                )
            )
            raise UserWarning(f"No one seems to own {bot.mention}")

        await ctx.send(
            embed=embeds.Embed(
                title=f"{bot.display_name}'s owner is",
                description=f"<@{owner}>",  # no need to get the full obj just to do a mention
            )
        )


def setup(bot):
    bot.add_cog(BotManager(bot))
