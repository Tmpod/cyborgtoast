#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
import random
import asyncio
import traceback
from toaster.utils import strings, exceptions, misc, pretty_list
from libneko import embeds
from libneko import Permissions as NekoPermissions
from textwrap import dedent


GREETINGS = (
    "Sup!",
    "Wassup!",
    "What's up!",
    "Greetings!",
    "Hello my good sir!",
    "Howdy!",
    "o/",
    ":wave:",
)

# MUST_HAVE_PERMS = {
#     "manage_messages": True,
#     "add_reactions": True,
#     "embed_links": True,
#     "external_emojis": True
# }

MUST_HAVE_PERMS = discord.Permissions(
    NekoPermissions.ADD_REACTIONS
    | NekoPermissions.MANAGE_MESSAGES
    | NekoPermissions.USE_EXTERNAL_EMOJIS
    | NekoPermissions.EMBED_LINKS
)


@misc.with_category()
class ModuleChecker:
    def __init__(self, bot):
        self.bot = bot

    # async def __global_check(self, ctx):
    #     if not ctx.guild:
    #         raise commands.NoPrivateMessage()
    #     elif ctx.cog and (type(ctx.cog).__name__ not in await ctx.bot.redis.smembers(f"guilds:{ctx.guild.id}:optional_modules")):
    #         raise exceptions.ModuleDisabled(ctx, type(ctx.cog).__name__)


@misc.with_category()
class Listeners:
    def __init__(self, bot):
        self.bot = bot

    def __global_check(self, ctx):
        guild = ctx.guild
        me = guild.me if guild is not None else ctx.bot.user
        perms = ctx.channel.permissions_for(me)

        self.bot.logger.debug(
            "I have %s perm value and should have %s"
            % (perms.value, MUST_HAVE_PERMS.value)
        )

        if perms.is_superset(MUST_HAVE_PERMS):
            return True

        raise commands.BotMissingPermissions(
            {p for p, v in perms.items() if getattr(perms, p, None) != v}
        )

    async def on_message(self, msg: discord.Message):
        if self.bot.creator.mentioned_in(msg):
            try:
                await self.bot.wait_until_ready()
                await msg.add_reaction("\N{BREAD}")
            except:
                pass
        elif self.bot.user.mentioned_in(msg):
            if len(msg.content.split()) == 1:
                prefixes = await misc.get_prefix(self.bot, msg)
                return await msg.channel.send(
                    embed=embeds.Embed(
                        description=(
                            f"{random.choice(GREETINGS)} {msg.author.mention}.\n"
                            f"My current prefixes for this guild are `{pretty_list(prefixes, sep='`, `')}`!\n"
                            f"Try `{prefixes[0]}help` to see my commands!"
                        ),
                        colour=misc.rand_hex(),
                    )
                )
            return await msg.add_reaction("\N{WAVING HAND SIGN}")

    async def on_guild_join(self, guild: discord.Guild):
        await self.bot.mongodb.guilds.insert_one(
            {"_id": guild.id, "prefixes": self.bot.default_prefixes}
        )
        await self.bot.redis.sadd(
            f"guilds:{guild.id}:prefixes", *self.bot.default_prefixes
        )

        em = embeds.Embed(
            title=":inbox_tray: __New Guild Join!__", description=f"```{guild.name}```"
        )
        em.set_footer(text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004")
        em.set_thumbnail(url=guild.icon_url)
        em.add_field(name="*Owner*", value=str(guild.owner))
        em.add_field(name="*ID*", value=str(guild.id))
        em.add_field(name="*Region*", value=str(guild.region))
        em.add_field(name="*Created on*", value=guild.created_at.strftime("%c"))
        em.add_field(name="*Verification Level*", value=str(guild.verification_level))
        em.add_field(name="*Content Filter*", value=str(guild.explicit_content_filter))
        em.add_field(name="*Member Count*", value=str(guild.member_count))
        em.add_field(
            name="*Channels*",
            value=f"Total:`{len(guild.channels)}`\nText:`{len(guild.text_channels)}`\nVoice:`{len(guild.voice_channels)}`",
        )
        em.add_field(name="*Emojis*", value=str(len(guild.emojis)))
        em.add_field(
            name="*Roles*", value=f"Total:`{len(guild.roles)}`\nTop:`{guild.roles[-1]}`"
        )
        em.add_field(name="*Default Channel*", value=str(guild.system_channel))
        em.add_field(
            name="*Features*",
            value=f"`{strings.pretty_list(guild.features, sep=', ')}`",
        )
        await self.bot.logs_channel.send(embed=em)
        
        emDM = embeds.Embed(
            title=f"CyborgToast joined {guild.name}!",
            description=f"Hey {guild.owner}! I'm very grateful you decided to have me on your server! I hope I'm of great use and that you have a fantastic time with your community!",
        )
        emDM.set_footer(
            text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004"
        )
        emDM.set_thumbnail(url=guild.icon_url)
        # emDM.add_field(
        #     name="Setup Status",
        #     value="Basic data all set! *If you want to enable the in-beta party module for your guild, contact my creator via `t.feedback <your message>` or via DM*",
        # )
        await guild.owner.send(embed=emDM)

    async def on_guild_remove(self, guild: discord.Guild):
        em = embeds.Embed(
            title=":outbox_tray: __New Guild Remove__",
            description=f"```{guild.name}```",
        )
        em.set_footer(text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004")
        em.set_thumbnail(url=guild.icon_url)
        em.add_field(name="*Owner*", value=str(guild.owner))
        em.add_field(name="*ID*", value=str(guild.id))
        em.add_field(name="*Region*", value=str(guild.region))
        em.add_field(name="*Created on*", value=guild.created_at.strftime("%c"))
        em.add_field(name="*Verification Level*", value=str(guild.verification_level))
        em.add_field(name="*Content Filter*", value=str(guild.explicit_content_filter))
        em.add_field(name="*Member Count*", value=str(guild.member_count))
        em.add_field(
            name="*Channels*",
            value=f"Total:`{len(guild.channels)}`\nText:`{len(guild.text_channels)}`\nVoice:`{len(guild.voice_channels)}`",
        )
        em.add_field(name="*Emojis*", value=str(len(guild.emojis)))
        em.add_field(
            name="*Roles*", value=f"Total:`{len(guild.roles)}`\nTop:`{guild.roles[-1]}`"
        )
        em.add_field(name="*Default Channel*", value=str(guild.system_channel))
        em.add_field(
            name="*Features*",
            value=f"`{strings.pretty_list(guild.features, sep=', ')}`",
        )
        await self.bot.logs_channel.send(embed=em)

    async def on_command_error(self, ctx: commands.Context, error):
        """This will handle any exception that is raised inside a command"""
        await self.bot.baker.err.catch_error(ctx, error)

    async def on_error(self, error):
        """Just logging the error"""
        error = error.__cause__ or error

        self.bot.logger.error(error, 
            # exc_info=True
            )

    async def on_command(self, ctx):
        """Ups the command invokation counter"""
        self.bot.cmd_counter += 1


def setup(bot):
    """Setting up both cogs in this file"""
    bot.add_cog(Listeners(bot))  # Setting up the Listeners cog
    bot.add_cog(ModuleChecker)  # Setting up the ModuleChecker cog
