#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands

from libneko import embeds
from textwrap import dedent
import traceback

from toaster.utils import (
    pretty_list,
    admins_or_creator,
    with_category,
    confirm_opts_full,
)


@with_category()
class Prefixes:
    def __init__(self, bot):
        self.bot = bot
        self.MAX_PREFIXES = 3
        self.MAX_PF_LENGTH = 4

    @commands.guild_only()
    @commands.group(
        name="prefix",
        aliases=("pf",),
        brief="Manage my prefixes!",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def prefix_group(self, ctx):
        """This group contains many custom server prefix utilities."""
        await self.list_prefixes.invoke(ctx)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    @prefix_group.command(
        name="list", aliases=("ls",), brief="Lists all prefixes for this server."
    )
    async def list_prefixes(self, ctx):
        """Sends a list with all the currently available prefixes for this server."""
        key = f"guilds:{ctx.guild.id}"
        guild_prefixes = await self.bot.redis.smembers(f"{key}:prefixes")
        selected_prefixes = guild_prefixes or self.bot.default_prefixes
        nl = "\n"
        em = embeds.Embed(
            title="Here are the current prefixes for this guild",
            description=f"`{pretty_list(selected_prefixes, sep=f'`{nl}`')}`",
            timestamp=None,
        )
        em.set_footer(
            text=f"You can add or remove prefixes by doing '{ctx.prefix}prefix add/remove <prefix>'"
        )
        await ctx.send(embed=em)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @admins_or_creator()
    @prefix_group.command(
        name="set", aliases=("s",), rief="Sets the prefix for this server"
    )
    async def set_prefix(self, ctx, *, prefixes: str):
        """Sets the prefix for this server overriding all the other prefixes"""
        if ctx.message.mentions:
            raise UserWarning(
                f"{self.bot.crossmark} You can't set a mentions as prefixes!"
            )

        prefixes = prefixes.split()

        if len(prefixes) > self.MAX_PREFIXES:
            raise UserWarning(
                f"{self.bot.crossmark} That's too many prefixes (max: **{self.MAX_PREFIXES}**)!"
            )

        for p in prefixes:
            if len(p) > self.MAX_PF_LENGTH:
                raise UserWarning(
                    f"{self.bot.crossmark} `{p}` is too big! It must be at max **{self.MAX_PF_LENGTH}** characters long."
                )

        key = f"guilds:{ctx.guild.id}"

        await self.bot.redis.delete(f"{key}:prefixes")
        await self.bot.redis.sadd(f"{key}:prefixes", *prefixes)
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"prefixes": prefixes}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully set the prefix for this server as `{pretty_list(prefixes)}` !",
                colour=discord.Colour.green(),
                timestamp=None,
            )
        )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @admins_or_creator()
    @prefix_group.command(
        name="add", aliases=("a",), brief="Adds a prefix to this server's prefix list."
    )
    async def add_prefix(self, ctx, *, prefixes: str):
        """
        Adds a prefix to the currently available prefixes for this server. 
        """
        if ctx.message.mentions:
            raise UserWarning(
                f"{self.bot.crossmark} You can't add a mentions as prefixes!"
            )

        prefixes = prefixes.split()

        if (
            len(prefixes) > self.MAX_PREFIXES - 1
        ):  # one prefix must already exist, at least
            raise UserWarning(
                f"{self.bot.crossmark} That's too many prefixes (max: **{self.MAX_PREFIXES}**)!"
            )

        for p in prefixes:
            if len(p) > self.MAX_PF_LENGTH:
                raise UserWarning(
                    f"{self.bot.crossmark} `{p}` is too big! It must be at max **{self.MAX_PF_LENGTH}** characters long."
                )

        key = f"guilds:{ctx.guild.id}"
        guild_prefixes = await self.bot.redis.smembers(f"{key}:prefixes")

        if len(guild_prefixes) + len(prefixes) > self.MAX_PREFIXES:  # we check it again
            raise UserWarning(
                f"{self.bot.crossmark} That's too many prefixes (max: **{self.MAX_PREFIXES}**)!"
            )

        for p in prefixes:
            if p in guild_prefixes:
                raise UserWarning(
                    f"{self.bot.crossmark} That prefix is already registered!"
                )

        await self.bot.redis.sadd(f"{key}:prefixes", *prefixes)
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"prefixes": guild_prefixes + prefixes}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully added `{pretty_list(prefixes)}` to this server's prefix list!",
                colour=discord.Colour.green(),
                timestamp=None,
            )
        )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @admins_or_creator()
    @prefix_group.command(
        name="remove",
        aliases=("rm",),
        brief="Removes a prefix to this server's prefix list.",
    )
    async def remove_prefix(self, ctx, *, prefixes: str):
        """Removes a prefix from the currently available prefixes for this server."""
        prefixes = prefixes.split()
        key = f"guilds:{ctx.guild.id}"
        guild_prefixes = await self.bot.redis.smembers(f"{key}:prefixes")

        if len(guild_prefixes) == 1:
            await ctx.invoke(self.reset_prefixes)

        for p in prefixes:
            if p not in guild_prefixes:
                raise UserWarning(
                    f"{self.bot.crossmark} `{p}` isn't a registered prefix!"
                )

        await self.bot.redis.srem(f"{key}:prefixes", *prefixes)
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id},
            {"$set": {"prefixes": list(set(guild_prefixes) - set(prefixes))}},
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully removed `{pretty_list(prefixes)}` from this server's prefix list!",
                colour=discord.Colour.green(),
                timestamp=None,
            )
        )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @admins_or_creator()
    @prefix_group.command(
        name="reset", aliases=("rs",), brief="Resets this server's prefixes."
    )
    async def reset_prefixes(self, ctx):
        """Resets the prefixes for this server back to the default ones."""
        key = f"guilds:{ctx.guild.id}:prefixes"

        await confirm_opts_full(self.bot, ctx.channel, ctx.author)

        await self.bot.redis.delete(key)
        await self.bot.redis.sadd(key, *self.bot.default_prefixes)
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"prefixes": self.bot.default_prefixes}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully reset this server's prefixes!",
                colour=discord.Colour.green(),
                timestamp=None,
            )
        )


def setup(bot):
    bot.add_cog(Prefixes(bot))
