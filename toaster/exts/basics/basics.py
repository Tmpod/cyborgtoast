#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
General basics
"""

import discord
from discord.ext.commands import Cog
from libneko import commands

from libneko import embeds
from toaster.utils import rand_hex, with_category


@with_category()
class Basics:
    """
    General basic commands
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=("i",), brief="Sends my invite link.")
    async def invite(self, ctx):
        """Sends my invite link. With this you can add me to your server!"""
        await ctx.send(
            embed=embeds.Embed(
                title=":incoming_envelope: Here's my invite link!",
                description=self.bot.config.toast.invite,
                colour=rand_hex(),
            )
        )

    @commands.cooldown(rate=1, per=60, type=commands.BucketType.user)
    @commands.command(aliases=("fb",), brief="Tell me your toughts about my service!")
    async def feedback(self, ctx, *, text: str):
        """Sends a feeback report to my creator."""
        emb = embeds.Embed(
            description=(
                f"**Guild:** {ctx.guild.name} ({ctx.guild.id})\n"
                f"**Channel:** {ctx.channel.name} ({ctx.channel.id})\n"
                f"```md\n{text}```"
            )
        )
        emb.set_author(name=f"{ctx.author} suggestion", icon_url=ctx.author.avatar_url)
        msg = await self.bot.suggestions_log.send(embed=emb)

        await ctx.send(
            embed=embeds.Embed(
                title=f"{self.bot.checkmark} Successfully sent suggestion to my creator!",
                description="Thanks for suggesting something to my creator! Now he'll take a look at your request and then give you an answer ASAP.",
                colour=discord.Colour.green(),
            )
        )

    @commands.command(
        name="linesofcode",
        aliases=("loc", "devhighscore"),
        brief="Gets the developer's high score (lines of code)",
    )
    async def loc_cmd(self, ctx):
        """Counts all the lines of code that my creator put time into."""
        await ctx.send(
            embed=embeds.Embed(
                description=(
                    f":tada: **`{self.bot.loc['header']['n_lines']}`** lines "
                    f"across **`{self.bot.loc['header']['n_files']}`** files!"
                )
            )
        )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Basics(bot))
