#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2018 Neko404NotFound
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in a$
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""
An extension that can be loaded 

Author:
    Tmpod

This is a modified version of what it's on libneko. Either way I'm the author of all this code.
"""

from discord.ext.commands import Cog
from libneko import commands as _commands

import libneko
from libneko import embeds, other, strings
from libneko.pag import navigator

from typing import Iterable, List

from random import choice

from toaster.utils import with_category, closable


def lower_string(arg: str):
    return arg.lower()


@with_category()
class Help:
    """
    Parameters:
        dm: Defines if the help message should be send thought dm or not. Default is set to False
        colours: Defines the list of colors that the help command will cycle through when making the
        command list or randomly choose from when making a specific help page.
        colors: An alias for the :attr:`colour` for the 'muricans' 

    Note:
        If the command in executed in dm, the command will act as if the dm value is set to True
    """

    def __init__(
        self,
        dm: bool = False,
        colors: Iterable[str] = None,
        colours: Iterable[str] = None,
    ):
        self.dm = dm
        if colours and colors:
            raise TypeError(
                "You must only provide either one of the colour parameters!"
            )
        if colors:
            colours = colors
        if colours:
            self.hex_values = []
            for c in colours:
                try:
                    self.hex_values.append(int(c, 16))
                except ValueError:
                    continue
        else:
            self.hex_values = None

    def _make_help_page(
        self, ctx: _commands.Context, cmd: _commands.Command, prefix: str
    ) -> libneko.Embed:
        """
        Makes a help page for the given command with the given prefix.
        """

        if self.hex_values:
            colour = choice(self.hex_values)
        else:
            colour = other.random_colour()

        title = f"`{cmd.qualified_name}` help page"
        if isinstance(cmd, _commands.Group):
            title = f"**[GROUP]** `{cmd.qualified_name}` help page"

        website_url = (
            f"{ctx.bot.config.toast.website_raw}{cmd.qualified_name.lower().replace(' ', '-')}.html"
            if not any((cmd.hidden, not cmd.enabled))
            else ""
        )

        emb = embeds.Embed(
            title=title,
            description=f"{cmd.brief}\n[***Website Link***]({website_url})",
            colour=colour,
            timestamp=None,
        )
        emb.add_field(name="Usage", value=f"```md\n{prefix}{cmd.signature}```")
        emb.add_field(
            name="Full Description", value=f"{cmd.help or '`No description set yet`'}"
        )

        buckets = getattr(cmd, "_buckets")
        if buckets:
            cooldown = getattr(buckets, "_cooldown")
            if cooldown:
                timeout = cooldown.per
                if timeout.is_integer():
                    timeout = int(timeout)

                emb.add_field(
                    name="Cooldown",
                    value=(
                        f"**Scope:** `{cooldown.type.name.title()}`\n"
                        f"**Rate:** `{cooldown.rate}` "
                        f"per `{timeout}` "
                        f"second{'s' if timeout else ''}"
                    ),
                )

        if cmd.cog_name is not None:
            category = ctx.bot.cogs[cmd.cog_name].category
        else:
            category = "Unsorted"

        emb.add_field(name="Category", value=f"`{category}`" or "`Unsorted`")

        if isinstance(cmd, _commands.Group):
            childgroups_short_info = {
                f"\N{BULLET} `{c.name}` - {c.brief}"
                for c in sorted(cmd.commands, key=str)
                if isinstance(c, _commands.Group)
            }
            childcmds_short_info = {
                f"\N{BULLET} `{c.name}` - {c.brief}"
                for c in sorted(cmd.commands, key=str)
                if isinstance(c, _commands.Command)
                and not isinstance(c, _commands.Group)
            }

            if childgroups_short_info:
                emb.add_field(
                    name="Subgroups",
                    value=strings.pretty_list(childgroups_short_info, sep="\n"),
                )
            emb.add_field(
                name="Subcommands",
                value=strings.pretty_list(childcmds_short_info, sep="\n"),
            )

        return emb

    def _make_help_pages(
        self,
        ctx: _commands.Context,
        cmds: List[_commands.Command],
        pages: List[libneko.Embed],
        category: str = None,
        step: int = 10,
    ) -> None:
        """
        Makes pages (sorted by cogs) for the command iterable given.
        """

        cursor = (
            1
        )  # Starting at one because we already used the first colour for the description page

        title = f"**{category}** Category"

        for i in range(0, len(cmds), step):
            if self.hex_values:
                if cursor <= len(self.hex_values) - 1:
                    colour = self.hex_values[cursor]
                else:
                    cursor = 0
            else:
                colour = other.random_colour()

            page = embeds.Embed(title=title, colour=colour, timestamp=None)
            # quest = "\N{BLACK QUESTION MARK ORNAMENT}"
            page.set_author(
                name=f"{ctx.bot.custom_emojis.i_book} {ctx.bot.user.name}'s Commands",
                icon_url=ctx.bot.user.avatar_url,
            )
            page.set_footer(
                icon_url=ctx.bot.user.avatar_url,
                text="Only commands you can run are visible",
            )

            next_commands = cmds[i : i + 10]
            for cmd in next_commands:
                name = cmd.name
                if isinstance(cmd, _commands.Group):
                    name = "**[GROUP]** " + cmd.name
                page.add_field(
                    name=name,
                    value=f"\N{EM SPACE}{cmd.brief}"
                    or "\N{EM SPACE}`No brief set yet`",
                )
            pages.append(page)

            cursor += 1

    @staticmethod
    def _get_categories(
        ctx,
        sorted_cmds: List[_commands.Command],
        unsorted_cmds: List[_commands.Command],
    ):
        categories = {}

        if unsorted_cmds:
            categories["Unsorted"] = unsorted_cmds

        for name, cog in ctx.bot.cogs.items():
            filtered_cog_cmds = [
                c for c in ctx.bot.get_cog_commands(name) if c in sorted_cmds
            ]
            try:
                categories[cog.category] += filtered_cog_cmds
            except KeyError:
                categories[cog.category] = filtered_cog_cmds

        return categories

    @_commands.cooldown(rate=1, per=5, type=_commands.BucketType.channel)
    @libneko.command(aliases=("h", "commands", "cmds"), brief="Shows this page")
    async def help(self, ctx: _commands.Context, *, query: lower_string = None):
        """
        Shows a brief paginated list of all commands available in this bot preceeded by an introductory page.
        Use the `commands`/`cmds` alias to skip that page.
        You can also specify a command to see more indepth details about it.
        """
        if self.dm:
            if ctx.guild is None:
                await ctx.send(
                    embed=embeds.Embed(
                        description="Commands have been sent to your DM", timestamp=None
                    )
                )
                ctx.guild = None

        all_commands = tuple(sorted(ctx.bot.commands, key=str))
        if ctx.author == ctx.bot.creator:
            filtered_commands = all_commands
        else:
            filtered_commands = []
            for cmd in all_commands:
                try:
                    if await cmd.can_run(ctx) and not cmd.hidden and cmd.enabled:
                        filtered_commands.append(cmd)
                except Exception:
                    continue

        if not query:
            if self.hex_values:
                f_colour = self.hex_values[0]
            else:
                f_colour = other.random_colour()

            pages = []

            if ctx.invoked_with not in ("commands", "cmds"):
                first_page = embeds.Embed(
                    title=f"{ctx.bot.custom_emojis.i_book} {ctx.bot.user.name}'s Help Page",
                    description=ctx.bot.description,
                    colour=f_colour,
                    timestamp=None,
                )
                first_page.set_thumbnail(url=ctx.bot.user.avatar_url)
                first_page.set_footer(
                    text=(
                        "Move to the next page with "
                        "\N{BLACK RIGHT-POINTING TRIANGLE} to start viewing the commands"
                    )
                )
                pages.append(first_page)

            filtered_unsorted_cmds = [c for c in filtered_commands if not c.cog_name]
            categories = self._get_categories(
                ctx, filtered_commands, filtered_unsorted_cmds
            )

            for cat, cmds in categories.items():
                self._make_help_pages(ctx, sorted(cmds, key=str), pages, cat)

            await navigator.EmbedNavigator(pages=pages, ctx=ctx).start()

        else:
            searched_command = ctx.bot.get_command(query)

            if searched_command is None or (
                searched_command.root_parent is not None
                and searched_command.root_parent not in filtered_commands
            ):
                filtered_unsorted_cmds = [
                    c for c in filtered_commands if not c.cog_name
                ]
                categories = self._get_categories(
                    ctx, filtered_commands, filtered_unsorted_cmds
                )
                if query in categories:
                    pages = []
                    self._make_help_pages(
                        ctx, sorted(categories[query], key=str), pages, query
                    )
                    await navigator.EmbedNavigator(pages=pages, ctx=ctx).start()

                else:
                    raise UserWarning(
                        f"{ctx.bot.crossmark} That isn't a valid command!"
                    )

            dialog = await ctx.send(
                embed=self._make_help_page(ctx, searched_command, ctx.prefix)
            )

            ctx.bot.loop.create_task(
                closable(ctx.bot, (ctx.message, dialog), ctx.author)
            )


def setup(bot):
    """Add the cog to the bot directly. Enables this to be loaded as an extension."""
    bot.remove_command("help")
    bot.add_cog(Help())
