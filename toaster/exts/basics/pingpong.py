#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands

from time import monotonic as t_monotonic
from libneko import embeds
import asyncio
import traceback
from toaster.utils import strings, misc
from textwrap import dedent


@misc.with_category()
class PingPong:
    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    async def ping_general(ctx):
        start = t_monotonic()
        msg = await ctx.send(
            embed=embeds.Embed(
                description="Pinging...", colour=discord.Colour.dark_grey()
            )
        )
        ack_hb = (t_monotonic() - start) * 1000
        bot_hb = (
            ctx.bot.latency or (sum(ctx.bot.latencies) / len(ctx.bot.latencies))
        ) * 1000

        return msg, ack_hb, bot_hb

    @staticmethod
    async def ping_redis(ctx, *, runs: int = 10, full: bool = False):
        db = ctx.bot.redis
        counter = runs
        if not full:
            tst_types = {
                "set": {"func": db.set, "args": ["latency", str(counter)]},
                "get": {"func": db.get, "args": ["latency"]},
            }
            results = {}
            for t in tst_types:
                counter = runs
                r_temp = []
                while counter:
                    start = t_monotonic()
                    await tst_types[t]["func"](*tst_types[t]["args"])
                    r_temp.append((t_monotonic() - start) * 1000)
                    counter -= 1
                results[t] = sum(r_temp) / runs

            return results

        test_mold = {
            "set": {
                "strings": {"func": db.set, "args": ["latency", "test"]},
                "hashes": {
                    "func": db.hmset,
                    "args": ["latency", "test", "foo", "bar", "baz", "asdf", "movies"],
                },
                "sets": {"func": db.sadd, "args": ["latency", "test", "foo", "bar"]},
            },
            "get": {
                "strings": {"func": db.get, "args": ["latency"]},
                "hashes": {
                    "func": db.hmset,
                    "args": ["latency", "test", "bar", "asdf", "movies"],
                },
                "sets": {"func": db.smembers, "args": ["latency"]},
            },
        }

        results = {}

        for m in test_mold:  # method: 'get'/'set'
            results[m] = {}
            for t in test_mold[m]:  # type: 'strings', 'hashes', 'sets'
                func = test_mold[m][t]["func"]
                args = test_mold[m][t]["args"]
                tests = []
                counter = runs
                await db.delete("latency")
                while counter:
                    start = t_monotonic()
                    await func(*args)
                    tests.append((t_monotonic() - start) * 1000)
                    counter -= 1
                results[m][t] = f"{(sum(tests) / runs):,.2f}"

        return results

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @commands.command(name="ping", aliases=("pg",), brief="Ping pong!")
    async def ping_command(self, ctx, service: str = "bot", options: str = None):
        """
        Pings the bot and gets it's latency and Discord's latency.
        You can specify the service that you want to ping or either pass the `--full` or `-f` option to ping all services.
        """
        service = service.lower()
        if options:
            options = options.lower()
        VALID_SERVICES = {
            "all": ...,
            "bot": self.ping_general,
            "redis": self.ping_redis,
        }
        VALID_OPTIONS = {
            "all": (None,),
            "bot": (None,),
            "redis": ("--full", "-f", None),
        }
        if service not in VALID_SERVICES:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} {service} isn't a valid service!",
                    colour=discord.Colour.red(),
                )
            )
        if {service, options} <= {"all"}:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} You must pass the `all` argument once!",
                    colour=discord.Colour.red(),
                )
            )
        if options not in VALID_OPTIONS[service]:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} `{options}` isn't a valid option for {service}!",
                    colour=discord.Colour.red(),
                )
            )
        if service == "all":
            msg, ack_hb, bot_hb = await self.ping_general(ctx)
            r_set, r_get = [*(await self.ping_redis(ctx)).values()]
            emb = embeds.Embed(
                title="Pong!",
                description="Here's a summary of my servicies' latencies!",
                colour=discord.Colour.blurple(),
            )
            emb.add_field(name="Bot Heartbeat", value=f"{bot_hb:,.2f}ms", inline=True)
            emb.add_field(name="ACK", value=f"{ack_hb:,.2f}ms", inline=True)
            emb.add_field(
                name="Redis Heartbeat",
                value=f"**SET:** {r_set:,.2f}µs\n**GET:** {r_get:,.2f}µs",
            )
            emb.set_footer(
                text=(
                    f"You can run '{ctx.prefix}ping redis -f' "
                    "to take a better look at redis' latency"
                )
            )
            return await msg.edit(embed=emb)
        if service == "bot":
            msg, ack_hb, bot_hb = await self.ping_general(ctx)
            return await msg.edit(
                embed=embeds.Embed(
                    description=(
                        "Pong! :ping_pong:\n"
                        f"**:heartbeat: Heartbeat:** `{bot_hb:,.2f}ms`\t"
                        f"**:file_cabinet: ACK:** `{ack_hb:,.2f}ms`"
                    ),
                    colour=discord.Colour.blurple(),
                )
            )
        if service == "redis":
            if options in ("--full", "-f"):
                results = await self.ping_redis(ctx, full=True)
                set_avg = (
                    f"{(sum([float(n) for n in [*results['set'].values()]]) / 3):,.2f}"
                )
                get_avg = (
                    f"{(sum([float(n) for n in [*results['get'].values()]]) / 3):,.2f}"
                )

                description = dedent(
                    f"""
                    Here's a little table with the results!
                    `
                    +===================+=====+
                    ‖ Tested Type ‖ SET ‖ GET ‖
                    +===================+=====+
                    +=========================+
                    ‖   Strings   ‖{results['set']['strings'].center(5)}‖{results['get']['strings'].center(5)}‖
                    +===================+=====+
                    ‖ Hash Fields ‖{results['set']['hashes'].center(5)}‖{results['get']['hashes'].center(5)}‖
                    +===================+=====+
                    ‖    Sets     ‖{results['set']['sets'].center(5)}‖{results['get']['sets'].center(5)}‖
                    +===================+=====+
                    +===================+=====+
                    ‖  Total Avg  ‖{set_avg.center(5)}‖{get_avg.center(5)}‖
                    +===================+=====+
                    `

                    *__Note:__ All values are in microseconds (µs)*
                    """
                )

                return await ctx.send(
                    embed=embeds.Embed(
                        title="Pong!", description=description, colour=0xD82A20
                    )
                )

            r_set, r_get = [*(await self.ping_redis(ctx)).values()]

            description = dedent(
                f"""
                Here's the average set/get heartbeats!

                **SET**: {r_set:,.2f}µs
                **GET**: {r_get:,.2f}µs
                """
            )

            return await ctx.send(
                embed=embeds.Embed(
                    title="Pong!", description=description, colour=0xD82A20
                )
            )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(PingPong(bot))
