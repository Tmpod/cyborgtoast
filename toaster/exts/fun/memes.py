#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
from libneko.pag import navigator
import asyncio
from toaster.utils import misc


@misc.with_category()
class Meeeeemes:
    def __init__(self, bot):
        self.bot = bot
        files = misc.list_files("./cogs/fun/" "memes")
        self.max = len([*files])

    @commands.group(
        name="memes",
        aliases=["meme", "mm"],
        brief="**[GROUP]** Don't let your memes be dreams.",
    )
    async def memes_group(self, ctx):
        pass

    @memes_group.group(
        name="library",
        aliases=["lib"],
        brief="Suggest a meme or see how many there are",
    )
    async def meme_library(self, ctx):
        pass

    @meme_library.command(name="count")
    async def meme_count(self, ctx):
        await ctx.send(
            embed=embeds.Embed(
                description=f":1234: The current number of memes is **{self.max}**",
                color=discord.Colour.purple(),
            )
        )

    @meme_library.command(name="suggest", aliases=["sg"], brief="Suggest a new meme!")
    async def suggest_meme(self, ctx, url: str):
        pass

    @memes_group.command(
        name="random",
        aliases=["rd"],
        brief="Get a random meme. May the RNG gods me with you",
    )
    async def random_meme(self, ctx):
        meme_id = random.randint(1, self.max)
        em = embeds.Embed(
            description=f"MEEEME No.{meme_id}", color=misc.rand_hex(), timestamp=None
        )
        try:
            await ctx.send(
                file=discord.File(
                    f"./memes/{meme_id}.jpeg", filename=f"MEEEME No.{meme_id}.jpeg"
                ),
                embed=em,
            )
        except FileNotFoundError:
            try:
                await ctx.send(
                    file=discord.File(
                        f"./memes/{meme_id}.png", filename=f"MEEEME No.{meme_id}.png"
                    ),
                    embed=em,
                )
            except FileNotFoundError:
                await ctx.send(
                    file=discord.File(
                        f"./memes/{meme_id}.jpg", filename=f"MEEEME No.{meme_id}.jpg"
                    ),
                    embed=em,
                )

    @memes_group.command(
        name="id", brief="Get a random meme. May the RNG gods me with you"
    )
    async def meme_by_id(self, ctx, id: int):
        try:
            if id <= max and id:
                await ctx.send(
                    file=discord.File(
                        f"./memes/{id}.jpeg", filename=f"MEEEME No.{id}.jpeg"
                    )
                )
            else:
                await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.crossmark} That's not a valid ID!",
                        color=discord.Colour.red(),
                    )
                )
        except FileNotFoundError:
            try:
                await ctx.send(
                    file=discord.File(
                        f"./memes/{id}.png", filename=f"MEEEME No.{id}.png"
                    )
                )
            except FileNotFoundError:
                await ctx.send(
                    file=discord.File(
                        f"./memes/{id}.jpg", filename=f"MEEEME No.{id}.jpg"
                    )
                )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Meeeeemes(bot))
