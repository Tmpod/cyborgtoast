#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
import json
from libneko.pag import navigator
import random
import aiohttp
from libneko import embeds

from toaster.utils import pretty_list, rand_hex, with_category, chk_or_crs


@with_category()
class Binds:
    STATES = {True: "ON", False: "OFF"}

    def __init__(self, bot):
        self.bot = bot
        self.bot.loop.create_task(self.finish_setup())

    async def finish_setup(self):
        async with await self.bot.baker.rsc.get("binds.json") as fp:
            self.BINDS = json.loads(await fp.read())

        async with await self.bot.baker.rsc.get("support_toast.png", mode="rb") as fp:
            self.WH_AVATAR = bytearray(await fp.read())

    @commands.guild_only()
    @commands.group(
        name="binds",
        aliases=("b",),
        brief="A little bind/emoji system ʕ•ᴥ•ʔ",
        invoke_without_command=True,
    )
    async def binds_group(self, ctx):
        """
        These Binds are kind of an emoji system, where u prefix the emoji name with `/`.
        Check the `list` subcommand for the complete list of available binds.
        It requires the bot to have the `Manage Webhooks` permission.
        """
        await self.binds_list.invoke(ctx)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @binds_group.command(
        name="list", aliases=("ls",), brief="List all available binds. (　＾∇＾)"
    )
    async def binds_list(self, ctx):
        """
        Lists all the available binds in a nice navigator.
        You can suggest binds with the `suggest` command!
        """
        state = await self.bot.redis.hget(f"guilds:{ctx.guild.id}", "binds") or "1"
        pages = []
        for b in range(0, len(self.BINDS), 10):
            page = embeds.Embed(
                title=f"**__`State:`__** {chk_or_crs(int(state))}",
                colour=rand_hex(),
                timestamp=None,
            )
            page.set_author(
                name="CyborgToast Binds List", icon_url=self.bot.user.avatar_url
            )
            page.set_footer(
                icon_url=self.bot.user.avatar_url,
                text="Thanks for using CyborgToast! | Made with ❤ by Tmpod#1004",
            )
            next_binds = list(self.BINDS.keys())[b : b + 10]
            for b in next_binds:
                value = (
                    pretty_list(self.BINDS[b], sep="`, `")
                    if isinstance(self.BINDS[b], list)
                    else self.BINDS[b]
                )
                page.add_field(name=b, value=f"`{value}`")
            pages.append(page)
        navigator.EmbedNavigator(pages=pages, ctx=ctx).start()

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @binds_group.command(
        name="toggle", aliases=("t",), brief="Toggles this feature On or Off"
    )
    async def toggle_binds(self, ctx):
        """
        Turns the binds feature On or Off depending on the current state.
        You can check out the state by doing the `binds` command.
        """
        binds = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"binds": 1}
            )
        ).get("binds")

        if binds is None:
            new = True
        else:
            new = not binds["state"]

        await self.bot.redis.hset(f"guilds:{ctx.guild.id}", "binds", int(new))
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"binds.state": new}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{chk_or_crs(new)} Toggled the binds **{self.STATES[new]}**!"
            )
        )

    async def on_message(self, message: discord.Message):
        if message.guild is None or await self.bot.redis.hget(
            f"guilds:{message.guild.id}", "binds"
        ) in (None, "0"):
            return

        if message.author.bot or (
            message.content.startswith("```") and message.content.endswith("```")
        ):
            return
        # bind = discord.utils.find(lambda b: b in message.content.split(), self.BINDS)
        all_binds = {b for b in self.BINDS if b in message.content.split()}
        # if not bind:
        if not all_binds:
            return
        else:
            if message.guild is None:
                return await message.channel.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.crossmark} This feature can only be used in a server!",
                        colour=discord.Colour.red(),
                        timestamp=None,
                    )
                )
            if not message.channel.permissions_for(message.guild.me).manage_webhooks:
                return await message.channel.send(
                    embed=embeds.Embed(
                        description="<:stop:469543643124989962> I do not have `MANAGE_WEBHOOKS`, which is a requirement for this feature! Please grant me that and try again.",
                        colour=discord.Colour.red(),
                        timestamp=None,
                    ),
                    delete_after=8,
                )

        # results = [random.choice(self.BINDS[bind]) for bind in all_binds if ]
        results = {}
        for bind in all_binds:
            if isinstance(self.BINDS[bind], tuple):
                results.update({f"{bind} ": random.choice(self.BINDS[bind])})
                continue
            results.update({f"{bind} ": self.BINDS[bind]})

        weeb = discord.utils.get(
            await message.channel.webhooks(), name="Binds Manager Weeb"
        )

        if not weeb:
            weeb = await message.channel.create_webhook(
                name="Binds Manager Weeb", avatar=self.WH_AVATAR
            )

        name = message.author.display_name
        if len(name) < 2:
            name = str(message.author)

        composed_message = f"{message.content} ".replace(
            tuple(results)[0], results[tuple(results)[0]] + " "
        )
        for bind in results:  # THIS IS AIDS.....
            composed_message = composed_message.replace(bind, results[bind] + " ")
        try:
            await message.delete()
        except:
            pass
        finally:
            await weeb.send(
                content=composed_message,
                username=name,
                avatar_url=message.author.avatar_url,
            )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Binds(bot))
