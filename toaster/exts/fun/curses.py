#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
from libneko.pag import navigator
from libneko import embeds
from time import monotonic as t_monotonic

from toaster.utils import with_category, EmojiConverter

BLESS_HUE = 0x379369

CURSE_DURATION = 1800  # 30 min.


@with_category()
class Curses:
    def __init__(self, bot):
        self.bot = bot
        self.jynxed = {}

    async def curse_task(self, gid: int, user: discord.Member, emoji):
        start = t_monotonic()
        while t_monotonic() - start < CURSE_DURATION:
            msg = await self.bot.wait_for(
                "message",
                check=lambda m: all((m.guild.id == gid, m.author.id == user.id)),
            )
            try:
                await msg.add_reaction(emoji)
            except (discord.Forbidden, discord.errors.NotFound):
                pass

        self.jynxed.pop(f"{user.id}@{gid}", None)

    @commands.cooldown(rate=1, per=15, type=commands.BucketType.category)
    @commands.command(
        name="curse",
        aliases=("oppugno", "jynx"),
        brief="Curse someone with an emoji and they shall never rest again...",
    )
    async def emoji_curse(self, ctx, user: discord.Member, emoji: EmojiConverter):
        """
        "Curses" a user with the given emoji, so that each message sent by that user is marked with the emoji in question.
        :smiling_imp:
        """
        # emoji = self.bot.get_emoji(int(emoji.split(':')[2].strip('>'))) if '<:' in emoji or '<a:' in emoji else emoji
        if user == ctx.author:
            raise UserWarning(f"{self.bot.crossmark} You cannot curse yourself!")

        if self.jynxed.get(f"{user.id}@{ctx.guild.id}"):
            raise UserWarning(
                f"{self.bot.crossmark} {user.mention} has already been cursed!"
            )

        try:
            await ctx.message.add_reaction(emoji)
        except:
            raise UserWarning(f"{self.bot.crossmark} I can't find that emoji!")

        curse = self.bot.loop.create_task(self.curse_task(ctx.guild.id, user, emoji))
        self.jynxed.update({f"{user.id}@{ctx.guild.id}": curse})

        await ctx.send(
            embed=embeds.Embed(
                description=(
                    f":purple_heart: {user.mention} has been cursed with {emoji}. "
                    f"It will fade away in {CURSE_DURATION / 60}min."
                ),
                color=discord.Colour.purple(),
            )
        )

    @commands.command(
        name="bless",
        aliases=("finitincantatem", "countercurse"),
        brief="Bless someone with your love and lift curses.",
    )
    async def emoji_bless(self, ctx, user: discord.Member):
        """
        Lifts a user's curse, ending with their certain agony... :green_heart:
        """
        if user == ctx.author and user != self.bot.creator:
            raise UserWarning(f"{self.bot.crossmark} You cannot countercurse yourself!")

        curse = self.jynxed.get(f"{user.id}@{ctx.guild.id}")

        if curse is None:
            raise UserWarning(f"{self.bot.crossmark} {user.mention} isn't curses!")

        curse.cancel()
        self.jynxed.pop(f"{user.id}@{ctx.guild.id}", None)

        await ctx.send(
            embed=embeds.Embed(
                description=f":green_heart: {user.mention} has been blessed and the curse has faided away!",
                color=BLESS_HUE,
            )
        )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Curses(bot))
