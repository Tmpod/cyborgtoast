#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
import json
from libneko.pag import navigator
import random
import aiohttp
from libneko import embeds

from toaster.utils import pretty_list, rand_hex, with_category, chk_or_crs


@with_category()
class WeebEmojis:
    STATES = {True: "ON", False: "OFF"}
    TOGGLE_EMOJI = "\N{JOYSTICK}"

    def __init__(self, bot):
        self.bot = bot
        self.bot.loop.create_task(self.finish_setup())

    async def finish_setup(self):
        async with await self.bot.baker.rsc.get("support_toast.png", mode="rb") as fp:
            self.WH_AVATAR = bytearray(await fp.read())

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @commands.command(
        name="webhookemojis",
        aliases=("weebemojis", "whem"),
        brief="Use animated emojis with webhooks!",
    )
    async def weebemojis_cmd(self, ctx):
        """
        This feature allows you to use animated emojis with webhooks in a similar fashion as the Binds.
        You can use this command to check the current settings for it and to toggle it On or Off by reaction with
        :joystick:.
        """
        weebemojis = (
            await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id})
        ).get("weebemojis")

        current_state = weebemojis["state"]

        emb = embeds.Embed(
            title="Current WebHook Emojis state:",
                description=f"{chk_or_crs(current_state) {self.STATES[current_state]}}"
            )
        emb.set_footer(text=f"You can toggle it by reacting with {self.TOGGLE_EMOJI}. Times out in 15s")

        dialog = await ctx.send(embed=emb)

        try:
            rct, usr = await bot.wait_for(
                "reaction_add",
                check=lambda r, u: r.emoji == self.TOGGLE_EMOJI
                and r.message.id == message.id
                and u == user,
                timeout=15,
            )
        except asyncio.TimeoutError:
            emb.set_footer(text="Timed out!")
            return await dialog.edit(embed=emb)

        if weebemojis is None:
            new = True
            update = {"weebemojis": {"state": new}}
        else:
            new = not current_state
            update = {"weebemojis.state": new}

        await self.bot.redis.hset(f"guilds:{ctx.guild.id}", "weebemojis", int(new))
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": update}
        )

        emb.set_footer(text="Timed out!")
        await dialog.edit(embed=emb)


    async def on_message(self, message: discord.Message):
        if message.guild is None or await self.bot.redis.hget(
            f"guilds:{message.guild.id}", "weebemojis"
        ) in (None, "0"):
            return

        if message.author.bot or (
            message.content.startswith("```") and message.content.endswith("```")
        ):
            return
