#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Nintendo Switch code storage
"""
import discord
from discord.ext.commands import Cog
from libneko import commands, embeds
import re

from toaster.utils import with_category


@with_category()
class NintendoSwitchCodes:
    def __init__(self, bot):
        self.bot = bot
        self.CODE_PATTERN = re.compile(
            r"^sw(?:-\d{4}){3}$", re.I
        )  # it will be upper cased either way
        self.SWITCH_LOGO = "https://i.ibb.co/MccYYCz/switch-dbg.jpg"

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.group(
        name="switch",
        aliases=("nsc",),
        case_insensitive=True,
        invoke_without_command=True,
        brief="Nintendo Switch code storage",
    )
    async def switch_codes(self, ctx, *, member: discord.User = None):
        """
        Store, get or remove your Nintendo Switch friend code!
        This is an easy way of sharing your switch code with people and to make new friends.
        It works across servers.
        """
        await ctx.invoke(self.get_code, member)

    @switch_codes.command(
        name="set", aliases=("s",), brief="Register your Nintendo Switch code"
    )
    async def set_code(self, ctx, *, code: str):
        """
        Store your Nintendo Switch codes in my database
        """
        if not self.CODE_PATTERN.match(code):
            raise UserWarning(f"{self.bot.crossmark} That code is invalid!")

        if await self.bot.mongodb.switch_codes.find_one({"_id": ctx.author.id}) is None:
            await self.bot.mongodb.switch_codes.insert_one(
                {"_id": ctx.author.id, "code": code.upper()}
            )
        else:
            await self.bot.mongodb.switch_codes.update_one(
                {"_id": ctx.author.id}, {"$set": {"code": code.upper()}}
            )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully set your code as **`{code}`**!",
                timestamp=None,
            )
        )

    @switch_codes.command(
        name="remove",
        aliases=("rm", "delete", "del"),
        brief="Unregister your Nintendo Switch code",
    )
    async def del_code(self, ctx):
        await self.bot.mongodb.switch_codes.delete_one({"_id": ctx.author.id})

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully removed your code!",
                timestamp=None,
            )
        )

    @switch_codes.command(
        name="get", aliases=("g",), brief="Get someone's Nintendo Switch code"
    )
    async def get_code(self, ctx, member: discord.User = None):
        if member is None:
            member = ctx.author

        data = await self.bot.mongodb.switch_codes.find_one({"_id": member.id})

        if data is None:
            desc = "`No registered code yet`"
        else:
            desc = f"```prolog\n{data['code']}```"  # highlights the 'SW'

        emb = embeds.Embed(
            title=f"{member.display_name}'s Switch friend code",
            description=desc,
            timestamp=None,
        )
        emb.set_thumbnail(url=self.SWITCH_LOGO)

        await ctx.send(embed=emb)


def setup(bot):
    bot.add_cog(NintendoSwitchCodes(bot))
