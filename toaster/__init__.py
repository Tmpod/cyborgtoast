#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
CyborgToast, delivered by Baker
"""

__author__ = "Tmpod aka French Toast, XLR or BonjourCroquette"
__contributors__ = (__author__,)

__license__ = "MIT"
__title__ = "CyborgToast"
__version__ = "0.9.0"

__repository__ = f"https://gitlab.com/{__author__.split(' aka ')[0]}/{__title__}.git"
__url__ = __repository__
