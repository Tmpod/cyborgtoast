
<!-- 
	What should be included in a good issue report?
		* A good title that summarizes the topic
		* A very detailed description of the issue/bug
		* Screenshots of the possibily unintended behaviour
		* Steps to reproduce the occurence in question

	Note: Do not change the default layout unless you really think it's necessary, 
	as issues that don't comply with such format will be denied almost instantly
 -->

### What will this add to the project?

*More toasters!*

### Why do you think this is a good addition?

*YEAH TOAST!*

### Are there any new implications?

*Butter is now obligatory*
