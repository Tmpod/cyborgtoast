
<!-- 
	IMPORTANT: Before opening an issue and if the matter isn't obviously a blatent bug,
	you should do some search on the wiki first, to check if what you believe is an
	unintended behaviour or not!

	What should be included in a good issue report?
		* A good title that summarizes the topic
		* A very good description of the issue/bug
		* Supposed behaviour
		* Actuall behaviour
		* Steps to reproduce the occurence in question
		* Screenshots of the possibily unintended behaviour

	Note: Do not change the default layout unless you really think it's necessary, 
	as issues that don't comply with such format will be denied almost instantly
 -->

### Issue description

*Something meaningfull about the issue*

### Expected behaviour

1. *You grab the toaster*
2. *You put bread in it*
3. *Golden and delicious toast pops up*

### Steps to reproduce

1. *You grab the toaster*
2. *You put bread in it*
3. *YOUR TOAST GETS BURNT! HELL BREAKS LOOSE! WATCH YOUR HEAD, MARK!*

### Screenshots

*I wanna see those sweet bread shots...*